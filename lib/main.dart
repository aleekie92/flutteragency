import 'dart:io';

import 'package:agency_banking/constants/Utils.dart';
import 'package:agency_banking/views/AccountOpening.dart';
import 'package:agency_banking/views/AccountActivation.dart';
import 'package:agency_banking/views/AgentAccount.dart';
import 'package:agency_banking/views/BalanceInquiry.dart';
import 'package:agency_banking/views/CashAccountDeposit.dart';
import 'package:agency_banking/views/CashTransfer.dart';
import 'package:agency_banking/views/ChangePin.dart';
import 'package:agency_banking/views/Dashboard.dart';
import 'package:agency_banking/views/CashIdDeposit.dart';
import 'package:agency_banking/views/GroupLoan.dart';
import 'package:agency_banking/views/LoanApplication.dart';
import 'package:agency_banking/views/LoanRepayment.dart';
import 'package:agency_banking/views/LoanStatus.dart';
import 'package:agency_banking/views/Login.dart';
import 'package:agency_banking/views/Ministatement.dart';
import 'package:agency_banking/views/Registration.dart';
import 'package:agency_banking/views/ShareDeposit.dart';
import 'package:agency_banking/views/Splash.dart';
import 'package:agency_banking/views/Withdraw.dart';
import 'package:agency_banking/views/imagetest.dart';
import 'constants/Network.dart';
import 'file:///F:/projects/android/agency_banking/lib/constants/printjob.dart';
import 'package:flutter/material.dart';
void main() {
  HttpOverrides.global = new DevHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Core-Cash',
      theme: ThemeData(
        scaffoldBackgroundColor: kBackgroundColor,
        primaryColor: kPrimaryColor,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
      routes: {
        "login": (context) => Login(),
        "dashboard": (context) => Dashboard(),
        "iddeposit": (context) => CashIdDeposit(),
        "accountdeposit": (context) => CashAccountDeposit(),
        "deposit": (context) => CashIdDeposit(),
        "withdraw": (context) => Withdraw(),
        "activation": (context) => AccountActivation(),
        "changepin": (context) => ChangePin(),
        "registration": (context) => Registration(),
        "cashtransfer": (context) => CashTransfer(),
        "accountopening": (context) => AccountOpening(),
        "grouploan": (context) => GroupLoan(),
        "balanceinquiry": (context) => BalanceInquiry(),
        "sharedeposit": (context) => ShareDeposit(),
        "ministatement": (context) => Ministatement(),
        "loanapplication": (context) => LoanApplication(),
        "loanrepayment": (context) => LoanRepayment(),
        "loanstatus": (context) => LoanStatus(),
        "myaccount": (context) => AgentAccount(),
        "printtest": (context)=>PrintDemo(),
        "testimage": (context)=>ImageTest(),
      },
    );
  }
}

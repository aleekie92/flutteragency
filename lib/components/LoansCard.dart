import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';

class LoansCard extends StatelessWidget{
  const LoansCard({
    Key key,
    this.image,
    this.title,
    this.pressed,
    this.colors,
  }) : super(key: key);
  final String image, title;
  final Function pressed;
  final Color colors;
  @override
  Widget build(BuildContext context) { Size size = MediaQuery.of(context).size;
  return Container(
    margin: EdgeInsets.all(kDefaultPadding / 10,),
    width: size.width * 0.5,
    child: Column(
      children: <Widget>[
        GestureDetector(
          onTap: pressed,
          child: Column(
            children: [
              Container(
                height: 60,
                width: 80,
                padding: EdgeInsets.all(kDefaultPadding / 4),
                decoration: BoxDecoration(
                  color: colors,
                  borderRadius: BorderRadius.all(
                      Radius.circular(30)),
                  image: DecorationImage(image: AssetImage(image)),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: kPrimaryColor1.withOpacity(0.23)),
                  ],
                ),

              ),

              Container(
                child: Text(
                  "$title",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
                alignment: Alignment.bottomCenter,
              )
            ],
          ),

        ),
      ],
    ),
  );
  }

}
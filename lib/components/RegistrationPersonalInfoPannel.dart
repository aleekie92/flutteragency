import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/BaseClass.dart';
import '../constants/BaseClass.dart';
import '../constants/Utils.dart';
import '../constants/Utils.dart';
import '../constants/Utils.dart';
import '../constants/Utils.dart';

class RegistrationPersonalInfoPannel extends StatefulWidget {
  _RegistrationPersonalInfoPannel createState() =>
      _RegistrationPersonalInfoPannel();

}

class _RegistrationPersonalInfoPannel
    extends State<RegistrationPersonalInfoPannel> {
  TextEditingController names = new TextEditingController();
  TextEditingController registrationFee = new TextEditingController();
  TextEditingController phoneno = new TextEditingController();
  TextEditingController idno = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController employerno = new TextEditingController();

  bool employerDetails = false;

//citizenship dropdown items
  List<String> citizenshipList = [];
  List<String> employerCodeList = [];
  @override
  List<DropdownMenuItem<String>> getDropDownMenuCitizenship(List<String> dataList) {
    List<DropdownMenuItem<String>> citizenshipItems = new List();
    if (dataList.isNotEmpty) {
      for (String citizenz in dataList) {
        // here we are creating the drop down menu items, you can customize the item right here
        // but I'll just use a simple text for this
        citizenshipItems
            .add(new DropdownMenuItem(value: citizenz, child: new Text(citizenz)));
      }
      return citizenshipItems;
    } else {
      print("seeing null");
    }
  }

  String _citizenship;

//dropdown items ends here

  //gender dropdown items
  List _titz = [
    "Mr",
    "Mrs",
    "Miss",
    "Ms",
  ];
  List<DropdownMenuItem<String>> _dropDownMenuTitle;

  @override
  List<DropdownMenuItem<String>> getDropDownTitleItems() {
    List<DropdownMenuItem<String>> titleItems = new List();
    for (String t in _titz) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      titleItems.add(new DropdownMenuItem(value: t, child: new Text(t)));
    }
    return titleItems;
  }

  String _title;

//dropdown items ends here

  //gender dropdown items
  List _gendz = [
    "Male",
    "Female",
  ];
  List<DropdownMenuItem<String>> _dropDownMenuGender;

  //String account;

  @override
  List<DropdownMenuItem<String>> getDropDownMenuGender() {
    List<DropdownMenuItem<String>> genderItems = new List();
    for (String g in _gendz) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      genderItems.add(new DropdownMenuItem(value: g, child: new Text(g)));
    }
    return genderItems;
  }

  String _gender;

//dropdown items ends here

  //marital dropdown items
  List _marital = [
    "Single",
    "Married",
    "Widower",
    "Widow",
  ];
  List<DropdownMenuItem<String>> _dropDownMenuMarital;

  //String account;

  @override
  List<DropdownMenuItem<String>> getDropDownMenuMarital() {
    List<DropdownMenuItem<String>> maritalItems = new List();
    for (String m in _marital) {
      maritalItems.add(new DropdownMenuItem(value: m, child: new Text(m)));
    }
    return maritalItems;
  }

  String _marital_status;
//dropdown items ends here

  //marital dropdown items
  @override
  List<DropdownMenuItem<String>> getDropDownMenuEmployerCode(List<String> dataList) {
    List<DropdownMenuItem<String>> employerCodeItems = new List();
    if (dataList.isNotEmpty) {
      for (String employerCode in dataList) {
        employerCodeItems
            .add(new DropdownMenuItem(value: employerCode, child: new Text(employerCode)));
      }
      return employerCodeItems;
    } else {
      print("seeing null");
    }
  }

  String _employer_code;

//dropdown items ends here

  //Date piker
  DateTime currentDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(1800),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }

  //date piker end here

  //radio buttons
  int _radioValue = 0;
  TextEditingController nameController = TextEditingController();

  //radio button ends

  @override
  void initState() {
    super.initState();

    _dropDownMenuGender = getDropDownMenuGender();
    _gender = _dropDownMenuGender[0].value;

    _dropDownMenuTitle = getDropDownTitleItems();
    _title = _dropDownMenuTitle[0].value;

    _dropDownMenuMarital = getDropDownMenuMarital();
    _marital_status = _dropDownMenuMarital[0].value;

  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextField(
          controller: idno,
          keyboardType: TextInputType.number,
          maxLength: 9,
          decoration: InputDecoration(labelText: 'id number'),
        ),
        Text(
          "Select Citizenship",
          style: TextStyle(
              color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
        ),
       loadCitizenship,
        TextField(
          controller: phoneno,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(labelText: 'phone number'),
        ),
        Text(
          "Select Title",
          style: TextStyle(
              color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
        ),
        new DropdownButton(
          /*value: accountDetails,
        items: _getAccountsItems(),
        onChanged: (value) => setState(() => accountDetails = value),*/
          value: _title,
          items: _dropDownMenuTitle,
          onChanged: titleChangedDropDownItem, //
        ),
        TextField(
          controller: names,
          keyboardType: TextInputType.text,
          textCapitalization: TextCapitalization.characters,
          decoration: InputDecoration(labelText: 'full names'),
        ),
        TextField(
          keyboardType: TextInputType.emailAddress,
          controller: email,
          decoration: InputDecoration(labelText: 'email address'),
        ),
        Text(
          "Select Gender",
          style: TextStyle(
              color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
        ),
        new DropdownButton(
          /*value: accountDetails,
        items: _getAccountsItems(),
        onChanged: (value) => setState(() => accountDetails = value),*/
          value: _gender,
          items: _dropDownMenuGender,
          onChanged: genderChangedDropDownItem, //
        ),
        Text(
          "Select Marital Status",
          style: TextStyle(
              color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
        ),
        new DropdownButton(
          /*value: accountDetails,
        items: _getAccountsItems(),
        onChanged: (value) => setState(() => accountDetails = value),*/
          value: _marital_status,
          items: _dropDownMenuMarital,
          onChanged: maritalStatusChangedDropDownItem, //
        ),
        TextField(
          keyboardType: TextInputType.number,
          controller: registrationFee,
          decoration: InputDecoration(labelText: 'registration fee'),
        ),

        Row(children: <Widget>[
          Expanded(
            child: Text(currentDate.toString()),
          ),
          RaisedButton(
            onPressed: () => _selectDate(context),
            child: Text('Select date'),
          ),
        ]),

        Text("Employed?"),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Radio(
              value: 0,
              groupValue: _radioValue,
              onChanged: _handleRadioValueChange,
            ),
            new Text(
              'No',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              value: 1,
              groupValue: _radioValue,
              onChanged: _handleRadioValueChange,
            ),
            new Text(
              'Yes',
              style: new TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        ),

        Visibility(
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: employerDetails,
          child: Text(
            "Select Employer",
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
          ),
        ),
       loadEmployerCode,
        Visibility(
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: employerDetails,
          child: TextField(
            controller: employerno,
            keyboardType: TextInputType.text,
            textCapitalization: TextCapitalization.characters,
            decoration: InputDecoration(labelText: 'Employer Number'),
          ),
        ),
        // ),
      ],
    );
  }

  Future<void> employercodeChangedDropDownItem(String selectedEmployerCode) async {
    setState((){
      _employer_code = selectedEmployerCode;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(memberemployer, _employer_code);
    await prefs.setString(memberemployercode, _employer_code.split(':')[0].trim());
  }

  Future<void> citizenshipChangedDropDownItem(String selectedCitizenship) async {
    setState((){
      _citizenship = selectedCitizenship;
    });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString(citizenship, _citizenship.split(':')[0].trim());
  }

  Future<void> titleChangedDropDownItem(String selectedTitle) async {
    setState(() {
      _title = selectedTitle;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(membertitle, _title);
  }

  Future<void> genderChangedDropDownItem(String selectedGender) async {
    setState((){
      _gender = selectedGender;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(membergender, _gender);
  }

  Future<void> maritalStatusChangedDropDownItem(String selectedMaritalStatus) async {
    setState((){
      _marital_status = selectedMaritalStatus;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(membermaritalstatus, _marital_status);
  }

  Future<void> _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          employerDetails = false;
          break;
        case 1:
          employerDetails = true;
          break;
      }
    });
  }

  get loadCitizenship{
      return  FutureBuilder(
          future: BaseClass.requestSyncDropdown(context, "3"),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );

            } else {
              try {
                citizenshipList.clear();
                for (int i = 0; i < snapshot.data["saccobarch"].length; i++) {
                  citizenshipList.add(snapshot.data["saccobarch"][i]["branchcode"]+" : "+snapshot.data["saccobarch"][i]["branchname"]);
                }
                return new DropdownButton(
                  value: _citizenship,
                  items: getDropDownMenuCitizenship(citizenshipList),
                  onChanged: citizenshipChangedDropDownItem,
                );
              } catch (e) {
                print("error found $e");
                //return Text(snapshot.data.toString());
              }
            }
          }
      );
  }

  get loadEmployerCode{
    return  FutureBuilder(
        future: BaseClass.requestSyncDropdown(context, "4"),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return CircularProgressIndicator(
              backgroundColor: Colors.red,
            );

          } else {
            try {
              employerCodeList.clear();
              for (int i = 0; i < snapshot.data["saccobarch"].length; i++) {
                employerCodeList.add(snapshot.data["saccobarch"][i]["branchcode"]+" : "+snapshot.data["saccobarch"][i]["branchname"]);
              }
              return new DropdownButton(
                value: _employer_code,
                items: getDropDownMenuEmployerCode(employerCodeList),
                onChanged: employercodeChangedDropDownItem,
              );
            } catch (e) {
              print("error found $e");
              //return Text(snapshot.data.toString());
            }
          }
        }
    );
  }
}

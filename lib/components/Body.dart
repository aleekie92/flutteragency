import 'package:agency_banking/components/TitleSection.dart';
import 'package:agency_banking/components/AgentProfilePannel.dart';
import 'package:agency_banking/components/LoansPannel.dart';
import 'package:agency_banking/components/TransactionCard.dart';
import 'package:agency_banking/components/TransactionPannel.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'RegistrationCard.dart';
import 'RegistrationPanel.dart';
import 'HeaderWithSearchBox.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            HeaderWithSearchBox(size: size),
            TitleSection(
              title: "",
              image: "assets/images/forward.png",
              color:  Colors.white,
              pressed: () {
              },
            ),
            RegistrationPanel(),
            TitleSection(
              title: "",
              image: "assets/images/forward.png",
              color:  Colors.white,
              pressed: () {},
            ),
            TransactionPannel(),
            SizedBox(height: 5.0),
            TitleSection(
              title: "",
              image: "assets/images/forward.png",
              color:  Colors.white,
              pressed: () {},
            ),
            LoansPannel(),
            SizedBox(height: 5.0),
            TitleSection(
              title: "",
              image: "assets/images/forward.png",
              color:  Colors.white,
              pressed: () {},
            ),
            AgentProfilePannel(),
            SizedBox(height: 30.0),
            SizedBox(height: kDefaultPadding,)
          ],
        ),
      ),
    );
  }
}

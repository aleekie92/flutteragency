import 'package:agency_banking/components/LoansCard.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:flutter/material.dart';

class LoansPannel extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          LoansCard(
            image: "assets/images/loan_repay.png",
            title: "Loan Repayment",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "loanrepayment");
            },
          ),



         /* LoansCard(
            image: "assets/images/laon_app1.png",
            title: "Loan Application",
            colors: Colors.white,
            pressed: () {
              //Navigator.pushNamed(context, "loanapplication");
              BaseClass.toast("work in progress....");
            },
          ),*/



      /*    LoansCard(
            image: "assets/images/loan_repay.png",
            title: "Grp repayment",
            colors: Colors.white,
            pressed: (){
             // Navigator.pushNamed( context, "grouploan");
              BaseClass.toast("work in progress....");
            },),*/



           LoansCard(
            image: "assets/images/loan_statuss.png",
            title: "Loan Balance",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "loanstatus");
              //BaseClass.toast("work in progress....");
            },
          ),
        ],
      ),
    );
  }

}
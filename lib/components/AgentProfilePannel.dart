import 'dart:convert';

import 'package:agency_banking/components/AgentProfileCard.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'file:///F:/projects/android/agency_banking/lib/constants/printjob.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AgentProfilePannel extends StatefulWidget {
  _AgentProfilePannel createState() => _AgentProfilePannel();
}

class _AgentProfilePannel extends State<AgentProfilePannel> {
  List<String> EndofDayAccounts = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          AgentProfileCard(
            image: "assets/images/agent_info.png",
            title: "My Account",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "myaccount");
            },
          ),
          AgentProfileCard(

            image: "assets/images/re_ports.png",
            title: "Daily Report",
            colors: Colors.white,
            pressed: () async {
              BaseClass.enableBluetooth();

              Future.delayed(Duration(seconds: 4),() async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                String saccomotto = prefs.getString("saccomoto") ?? "";
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PrintDemo(
                        transWhat: "EOD",
                        memberno: "",
                        amt: 0,
                        title: "END OF DAY REPORT",
                        depositorphone: "",
                        depositorname: "",
                        deviceid: prefs.getString("imei") ?? "",
                        agentname: prefs.getString("accname") ?? "",
                        refno: "",
                        sacconame: prefs.getString("sacconame") ?? "",
                        idNo: "",
                        transactiondate: "",
                        decription: "",
                        depbyy: "",
                        moto: saccomotto,
                      ),
                    ));
              }
              );
            },
          ),
          AgentProfileCard(
            image: "assets/images/print_last.png",
            title: "Last Transaction",
            colors: Colors.white,
            pressed: () async {
              BaseClass.enableBluetooth();
              Future.delayed(Duration(seconds: 3),(){
                requestLastTransaction(context);
              }
              );
            },
          ),
        ],
      ),
    );
  }

  requestLastTransaction(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String terminalId = prefs.getString("imei") ?? "";
    String agentNamee = prefs.getString("accname") ?? "";
    String saccoName = prefs.getString("sacconame") ?? "";
    String saccomotto = prefs.getString("saccomoto") ?? "";

    ProgressDialog pr = ProgressDialog(context);
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        message: 'Fetching, please wait......',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    var url = Uri.parse(Network.URL + "GetAgentLastTransaction");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map lastTransactionBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
    };

    String body = json.encode(lastTransactionBlock);
    await pr.show();
    Response response = await post(url, headers: header, body: body);
    var responseData = jsonDecode(response.body);

    if (responseData['is_successful']) {
      String rnumber = responseData['receiptno'];
      String trantype = responseData['transactiontype'];
      String saccname = responseData['sacconame'];
      String amtt = responseData['amount'];
      String date = responseData['transactiondate'];
      String desct = responseData['Description'];
      String accno = responseData['AccountNo'];
      String accname = responseData['AccountName'];
      String depby = responseData['DepositedBy'];

      DateTime now = DateTime.parse(date);
      String formattedDate = DateFormat(date).format(now);

      await pr.hide();
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PrintDemo(
              transWhat: "LT",
              memberno: accno,
              amt: int.parse(amtt),
              title: trantype,
              depositorphone: "",
              depositorname: accname,
              deviceid: terminalId,
              agentname: agentNamee,
              refno: rnumber,
              sacconame: saccoName,
              idNo: "",
              transactiondate: formattedDate,
              decription: desct,
              depbyy: depby,
              moto: saccomotto,
            ),
          ));
    } else {
      await pr.hide();
      BaseClass.alertDialog(context, "Error!", "failed", "Close","assets/images/error.jpg");
    }
  }
}

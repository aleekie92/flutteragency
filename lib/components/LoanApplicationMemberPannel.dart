import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoanApplicationMemberPannel extends StatefulWidget {
  _LoanApplicationMemberPannel createState() => _LoanApplicationMemberPannel();
}

class _LoanApplicationMemberPannel extends State<LoanApplicationMemberPannel> {
  TextEditingController txtID = new TextEditingController();
  TextEditingController txtAmount = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool loanprogressvisible = false;

  //dropdown items
  List _accounts = [
    "S000000007",
    "S010000007",
    "S020000007",
    "S060000007",
  ];
  List<DropdownMenuItem<String>> _dropDownMenuAccounts;
  String account;
  @override
  void initState() {
    _dropDownMenuAccounts = getDropDownMenuItems();
    account = _dropDownMenuAccounts[0].value;
    super.initState();
  }
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> accountItems = new List();
    for (String acc in _accounts) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      accountItems.add(new DropdownMenuItem(value: acc, child: new Text(acc)));
    }
    return accountItems;
  }
  String _account;
//dropdown items ends here


  String nametextholder = '***********';
  String phonetextholder = '***********';


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,width: 250,
        height: 400,
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: new Column(
            children: <Widget>[
              memberDetails,

            ],
          ),
        ),
      ),
    );
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.number,
                maxLength: 10,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  setState(() {
                    loanprogressvisible = true;
                  });
                  searchFuntionality();
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      CircleAvatar(
        radius: 40,
        backgroundImage: AssetImage("assets/images/user.jpg"),
      ),
      Text(
        "Select Loan product",
        style: TextStyle(
            color: Colors.blue,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      new DropdownButton(
        value: _account,
        items: _dropDownMenuAccounts,
        onChanged: productsChangedDropDownItem,
      ),
      new ListTile(
        leading: const Icon(Icons.monetization_on_outlined),
        title: new TextField(
          keyboardType: TextInputType.number,
          decoration: new InputDecoration(
            hintText: "Enter Amount",
          ),
        ),
      ),

    ]);
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: loanprogressvisible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality() async {
    var response =
    await BaseClass.fetchMemberDetails(txtID.text.toString(), 0, context);

    try {
      if (response != null) {
        if(response['memberName'].toString() == "" || response['memberPhone'].toString() == ""){
          BaseClass.toast("Confirm ID number and try again...");
          setState(() {
            loanprogressvisible = false;
          });
        }else {
          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            loanprogressvisible = false;
          });
        }
      } else {
        setState(() {
          loanprogressvisible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        loanprogressvisible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [

            new Text(nametextholder.toUpperCase()),new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(fontSize: 14,fontWeight: FontWeight.bold,color: kPrimaryColor1),
            ),
            Spacer(),
            showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }
  void productsChangedDropDownItem(String selectedAccount) {
    print("Selected acc $selectedAccount, we are going to refresh the UI");
    setState(() {
      _account = selectedAccount;
    });
  }
}

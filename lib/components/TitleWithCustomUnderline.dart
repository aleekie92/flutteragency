import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';

class TitleWithCustomUnderline extends StatelessWidget {
  const TitleWithCustomUnderline({
    Key key,
    this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 15,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: kDefaultPadding / 6),
            child: Text(
              text,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';

import '../constants/BaseClass.dart';

class RegistrationPhysicalAddressPannel extends StatefulWidget{
_RegistrationPhysicalAddressPannel createState()=>_RegistrationPhysicalAddressPannel();
}

class _RegistrationPhysicalAddressPannel extends State<RegistrationPhysicalAddressPannel>{

  TextEditingController city = new TextEditingController();
  TextEditingController market = new TextEditingController();
  //citizenship dropdown items
  List<String> BranchesList = [];
  @override
  List<DropdownMenuItem<String>> getDropDownMenuBranches(List<String> dataList) {
    List<DropdownMenuItem<String>> citizenshipItems = new List();
    if (dataList.isNotEmpty) {
      for (String citizenz in dataList) {
        citizenshipItems
            .add(new DropdownMenuItem(value: citizenz, child: new Text(citizenz)));
      }
      return citizenshipItems;
    } else {
      print("seeing null");
    }
  }
  String _nearestbranch;
//dropdown items ends here

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   return Column(
     children: <Widget>[
       Text(
         "Select Branch",
         style: TextStyle(
             color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 15),
       ),
       loadBranches,
       TextField(
         controller: city,
         keyboardType: TextInputType.text,
         textCapitalization: TextCapitalization.characters,
         decoration: InputDecoration(labelText: 'city/town'),
       ),
       TextField(
         controller: market,
         keyboardType: TextInputType.text,
         textCapitalization: TextCapitalization.characters,
         decoration: InputDecoration(labelText: 'nearest market'),//select  fetched
       ),
     ],
   );
  }
  void branchesChangedDropDownItem(String selectedBranch) {
    setState(() {
      _nearestbranch = selectedBranch;
    });
  }


  get loadBranches{
    return  FutureBuilder(
        future: BaseClass.requestSyncDropdown(context, "1"),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return CircularProgressIndicator(
              backgroundColor: Colors.red,
            );

          } else {
            try {
              BranchesList.clear();
              for (int i = 0; i < snapshot.data["saccobarch"].length; i++) {
                BranchesList.add(snapshot.data["saccobarch"][i]["branchcode"]+" : "+snapshot.data["saccobarch"][i]["branchname"]);
              }
              return new DropdownButton(
                value: _nearestbranch,
                items: getDropDownMenuBranches(BranchesList),
                onChanged: branchesChangedDropDownItem,
              );
            } catch (e) {
              print("error found $e");
              //return Text(snapshot.data.toString());
            }
          }
        }
    );
  }
}
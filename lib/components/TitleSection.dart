import 'package:agency_banking/components/TitleWithCustomUnderline.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';

class TitleSection extends StatelessWidget{
  const TitleSection({
    Key key, this.title, this.pressed, this.image, this.color
  }) : super(key: key);
  final String title,image;
  final Function pressed;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding:  EdgeInsets.only(left: size.width * 0.4,right: 0),
      child: Row(
        children: [
          TitleWithCustomUnderline(
              text: title,
          ),
         Spacer(),
          //Image(image: AssetImage(image)),
        //Image(image: AssetImage(image)),
         /* FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)
              ),
              color: color,
              onPressed: pressed,
              child: Image(image: AssetImage(image)),
          )*/
        ],
      ),
    );
  }
}
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:flutter/material.dart';
import 'RegistrationCard.dart';

class RegistrationPanel extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.all(2.0),
      child: Row(
        children: [
         /* RegistrationCard(
            image: "assets/images/registrationn.png",
            title: "Customer Registration",
            colors: Colors.white,
            pressed: () {
              //Navigator.pushNamed(context, "registration");
              BaseClass.toast("work in progress....");
            },
          ),*/
          RegistrationCard(
            image: "assets/images/change_pin.png",
            title: "Member Activation",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "activation");
            },
          ),
          RegistrationCard(
            image: "assets/images/passwordd.png",
            title: "Member Pin Change",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "changepin");
            },
          ),
        ],

      ),
    );
  }
}
import 'package:agency_banking/components/TransactionCard.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransactionPannel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          TransactionCard(
            image: "assets/images/deposit_icon.png",
            title: "Cash Deposit",
            colors: Colors.white,
            pressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              if (prefs.getString("corporateno") == "CAP002") {
                Navigator.pushNamed(context, "accountdeposit");
              } else {
                identificationMethod(context);
              }
            },
          ),
          TransactionCard(
            image: "assets/images/withdrawal_icon.png",
            title: "Cash Withdraw",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "withdraw");
            },
          ),



          TransactionCard(
            image: "assets/images/share_depo.png",
            title: "Share Deposit",
            colors: Colors.white,
            pressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              if (prefs.getString("corporateno") == "CAP002") {
                BaseClass.toast("Module not enabled");
              } else {
                Navigator.pushNamed(context, "sharedeposit");
              }
            },
          ),


          TransactionCard(
            image: "assets/images/bal_ance.png",
            title: "Balance Enquiry",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "balanceinquiry");
            },
          ),
          TransactionCard(
            image: "assets/images/mini_state.png",
            title: "Ministatement",
            colors: Colors.white,
            pressed: () {
              Navigator.pushNamed(context, "ministatement");
            },
          ),

          /*TransactionCard(
            image: "assets/images/farmer_reg.png",
            title: "Acc Opening",
            colors: Colors.white,
            pressed: (){
              //Navigator.pushNamed( context, "accountopening");
              BaseClass.toast("work in progress....");
            },),

          TransactionCard(
            image: "assets/images/transferr.png",
            title: "Cash Transfer",
            colors: Colors.white,
            pressed: (){
              //Navigator.pushNamed( context, "cashtransfer");
              BaseClass.toast("work in progress....");
            },),

          TransactionCard(
            image: "assets/images/print_last.png",
            title: "Receipt Reprint",
            colors: Colors.white,
            pressed: (){
              //Navigator.pushNamed( context, "reprintreceipt");
              BaseClass.toast("work in progress....");
            },),*/
        ],
      ),
    );
  }

  identificationMethod(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 130,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              SizedBox(height: 15.0),
              Text(
                "Select Identification Method".toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "ID Number",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.pushNamed(context, "iddeposit");
                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Account No",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.pushNamed(context, "accountdeposit");
                      } //submit,
                      ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }
}

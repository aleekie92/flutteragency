import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

String agentName = "-";
String agentAccount = "-";

class HeaderWithSearchBox extends StatefulWidget {
  const HeaderWithSearchBox({Key key, @required this.size}) : super(key: key);
  final Size size;

  _HeaderWithSearchBox createState() => _HeaderWithSearchBox();
}

class _HeaderWithSearchBox extends State<HeaderWithSearchBox> {
  @override
  Widget build(BuildContext context) {
    getAgentInfo();
    final Size size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: kDefaultPadding * 0.5),
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  left: kDefaultPadding,
                  right: kDefaultPadding,
                  bottom: 16 + kDefaultPadding,
                ),
                height: size.height * 0.2 - 20,
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36)),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        '$agentName' + " - " + '$agentAccount',
                        //"TEST AGENT NAME-AGENT001",
                        style: Theme.of(context).textTheme.headline5.copyWith(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      ),
                      flex: 1,
                    ),
                    Spacer(),
                    //replace with image later
                    /* Expanded(
                      child: new Text(
                        "Logout".toUpperCase(),
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ),*/

                    CircleAvatar(
                        backgroundColor: Colors.white,
                        child: IconButton(
                          icon: const Icon(Icons.refresh),
                          onPressed: () {
                            BaseClass.disableBluetooth();
                          },
                        )),

                    /* Text(
                      "TEST SACCO LIMITED",overflow: TextOverflow.fade,
                      style: Theme.of(context).textTheme.headline5.copyWith(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),flex: 1,
                    ),*/
                    //Image.asset("assets/images/user.png")
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
                  padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
                  height: 44,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 10),
                          blurRadius: 50,
                          color: kPrimaryColor.withOpacity(0.23)),
                    ],
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            hintText: "Search",
                            hintStyle: TextStyle(
                              color: kPrimaryColor.withOpacity(0.5),
                            ),
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            //suffixIcon: SvgPicture.asset("assets/icons/search.svg"),
                          ),
                        ),
                      ),
                      SvgPicture.asset("assets/icons/search.svg")
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

Future<void> getAgentInfo() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  agentName = prefs.getString("accname") ?? "";
  agentAccount = prefs.getString("agentaccount") ?? "";
}

import 'dart:convert';

import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:typed_data';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(PrintDemo());

class PrintDemo extends StatefulWidget {
  const PrintDemo({
    Key key,
    this.transWhat,
    this.memberno,
    this.amt,
    this.title,
    this.depositorphone,
    this.depositorname,
    this.deviceid,
    this.agentname,
    this.refno,
    this.sacconame,
    this.idNo,
    this.transactiondate,
    this.decription,
    this.depbyy,
    this.moto,
    this.accountname,
    this.loantype,
  }) : super(key: key);

  final String transWhat;
  final String memberno;
  final int amt;
  final String title;
  final String depositorphone;
  final String depositorname;
  final String deviceid;
  final String agentname;
  final String refno;
  final String sacconame;
  final String idNo;
  final String transactiondate;
  final String decription;
  final String depbyy;
  final String moto;
  final String accountname;
  final String loantype;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<PrintDemo> {

  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  int count = 0;
  String printt="Print";

  List<BluetoothDevice> _devices = [];
  List<String> ministatementPostingDate = [];
  List<String> ministatementDescription = [];
  List<String> ministatementAmount = [];

  List<String> eodRefNo = [];
  List<String> eodAmount = [];
  List<String> eodTransactionType = [];

  BluetoothDevice _device;

  bool _connected = false;
  bool _pressed = false;
  String pathImage;
  ProgressDialog prr;
  int _radioValue = 0;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    initSavetoPath();

  }

  Future<void> getPairedDevices() async {
    List<BluetoothDevice> devices = [];
    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException {
      print("Error");
    }
    if (!mounted) {
      return;
    }
    setState(() {
      _devices = devices;
    });
  }

  initSavetoPath()async{
    final filename = 'logo.png';
    var bytes = await rootBundle.load("assets/images/logo.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    writeToFile(bytes,'$dir/$filename');
    setState(() {
      pathImage='$dir/$filename';
    });
  }
  Future<void> initPlatformState() async {
    List<BluetoothDevice> devices = [];

    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException {
      // TODO - Error
    }

    bluetooth.onStateChanged().listen((state) {
      switch (state) {
        case BlueThermalPrinter.CONNECTED:
          setState(() {
            _connected = true;
            _pressed = false;
          });
          break;
        case BlueThermalPrinter.DISCONNECTED:
          setState(() {
            _connected = false;
            _pressed = false;
          });
          break;
        default:
          print(state);
          break;
      }
    });

    if (!mounted) return;
    setState(() {
      _devices = devices;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      elevation: 0,
      backgroundColor: colorPrimary,
      child: contentBox(),
    );
  }

  contentBox() {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Container(
          child: ListView(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                child: Text(
                  "SELECT PRINTER AND CONNECT.",
                  style: TextStyle(fontSize: 17, color: kPrimaryColor1),
                ),
              ),

              Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Printer:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    DropdownButton(
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() => _device = value),
                      value: _device,
                    ),
                    RaisedButton(
                      onPressed: _pressed ? null: _connected ? _disconnect : _connect,
                      child: Text(_connected ? 'Disconnect' : 'Connect'),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 45.0,
              ),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: kPrimaryColor1,
                child: MaterialButton(
                  minWidth: 50, //MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                    setState(() {
                      count++;
                    });
                    if(count==1){
                      _connected ? CustomerCopy() : null;
                      printt = "Reprint Copy";
                    }else{
                      _connected ? AgentCopy() : null;
                    }
                  },
                  child: Text(
                    ('$printt'),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: 60.0,
              ),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: kPrimaryColor,
                child: MaterialButton(
                  minWidth: 50, //MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                    BaseClass.disableBluetooth();
                    Navigator.of(context).pop();
                    Navigator.pushNamed(context, "dashboard");
                  },
                  child: Text(
                    ("Exit Printing"),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),

      ),
    );
  }

  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devices.isEmpty) {
      //BaseClass.toast("Turn ON Blue tooth");
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }

  void _connect() {
    if (_device == null) {
      BaseClass.toast('No device selected.');
    } else {
      bluetooth.isConnected.then((isConnected) {
        if (!isConnected) {
          bluetooth.connect(_device).catchError((error) {
            setState(() => _pressed = false);
          });
          setState(() => _pressed = true);
        }
      });
    }
  }

  void _disconnect() {
    bluetooth.disconnect();
    setState(() => _pressed = false);
  }

//write to app path
  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  void CustomerCopy() async{
    bluetooth.isConnected.then((isConnected) async {
      if (isConnected) {
        if (widget.transWhat == "D") {
          bluetooth.printNewLine();
          bluetooth.printCustom("............................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 0);
          bluetooth.printCustom(widget.title, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
          bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
          bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
          bluetooth.printCustom("Description :" + widget.decription.toString(), 1, 0);
          bluetooth.printCustom("Deposited By :" + widget.depbyy, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Customer Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("please come back again", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("......customer copy......", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
        }
        if (widget.transWhat == "W") {
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
          bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.title, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
          bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
          bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
          bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("ID :" + "...........................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Customer Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("please come back again", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("......customer copy......", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
        }

        if (widget.transWhat == "LR") {
          bluetooth.printNewLine();
          bluetooth.printCustom(".............................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
          bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.title, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
          bluetooth.printCustom("Phone Number :" + widget.depositorphone, 1, 0);
          bluetooth.printCustom("Loan type :" + widget.loantype, 1, 0);
          bluetooth.printCustom("Loan number :" + widget.memberno, 1, 0);
          bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
          bluetooth.printCustom("Paid By :" + widget.depbyy, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Customer Signature :" + ".............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("please come back again", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("......customer copy......", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();

        }
        if (widget.transWhat == "SD") {
          bluetooth.printNewLine();
          bluetooth.printCustom("..........................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
          bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.title, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
          bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
          bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
          bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();

          bluetooth.printCustom("Customer Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("please come back again", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("......customer copy......", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
        }
        if (widget.transWhat == "M") {
          prr = BaseClass.progreDialog(context);
          prr.show();
          SharedPreferences prefs = await SharedPreferences.getInstance();
          var url = Uri.parse(Network.URL + "GetMiniStatement");
          Map<String, String> header = {
            "Content-Type": "application/json; charset=utf-8"
          };

          Map withdrawalBlock = {
            "authorization_credentials": {
              "api_key": "12345",
              "token": Network.appToken
            },
            "corporate_no": prefs.getString("corporateno") ?? "",
            "accountidentifier": widget.idNo,
            "accountno": widget.memberno,
            "accountidentifiercode": "0",
            "agentid": prefs.getString("agtcode") ?? "",
            "terminalid": prefs.getString("imei") ?? "",
            "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
            "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
            "date": BaseClass.getFormattedDate()
          };
          String body = json.encode(withdrawalBlock);
          Response response = await post(url, headers: header, body: body);
          if (response != null) {
            prr.hide();
            var responseData = jsonDecode(response.body);
            if (responseData['is_successful']) {
              prr.hide();
              ministatementPostingDate.clear();
              ministatementDescription.clear();
              ministatementAmount.clear();
              for (int i = 0; i < responseData["transactions"].length; i++) {
                ministatementPostingDate
                    .add(responseData["transactions"][i]["posting_date"]);
                ministatementDescription
                    .add(responseData["transactions"][i]["description"]);
                ministatementAmount
                    .add(responseData["transactions"][i]["amount"].toString());
              }

              bluetooth.printNewLine();
              bluetooth.printCustom("...........................", 1, 0);
              bluetooth.printNewLine();
              bluetooth.printCustom(widget.sacconame, 1, 1);
              bluetooth.printCustom("Terminal ID :" + widget.deviceid, 1, 1);
              bluetooth.printCustom(
                  "Date :" + BaseClass.getFormattedDate(), 1, 1);
              bluetooth.printCustom(widget.title, 1, 1);
              bluetooth.printNewLine();
              for (int i = 0; i < ministatementPostingDate.length; i++) {
                bluetooth.printCustom("Date :" + ministatementPostingDate[i], 1, 0);
                bluetooth.printCustom(ministatementDescription[i], 1, 0);
                bluetooth.printCustom("Amount :" + ministatementAmount[i], 1, 0);
                bluetooth.printNewLine();
              }
              bluetooth.printNewLine();
              bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
              bluetooth.printNewLine();
              bluetooth.printNewLine();
              bluetooth.printCustom(widget.moto, 1, 1);
              bluetooth.printNewLine();
              bluetooth.printCustom("please come back again", 1, 1);
              bluetooth.printNewLine();
              bluetooth.printNewLine();
              BaseClass.disableBluetooth();
              Navigator.pushNamed(context, "dashboard");
            } else {
              prr.hide();
              BaseClass.alertDialog(
                  context, "Error!", responseData['error'], "Ok","assets/images/error.jpg");
              BaseClass.disableBluetooth();
              Navigator.pushNamed(context, "dashboard");
            }
          }
        }
        if (widget.transWhat == "LT") {
          bluetooth.printNewLine();
          bluetooth.printCustom("..........................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Reprint Date :" + BaseClass.getFormattedDate(), 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("RECEIPT REPRINT", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("TransactionType :" + widget.title, 1, 0);
          bluetooth.printCustom("Copy Receipt number :" + widget.refno, 1, 0);
          bluetooth.printCustom("Description :" + widget.decription, 1, 0);
          bluetooth.printCustom( "Acc Number :" + widget.memberno.toString(), 1, 0);
          //bluetooth.printCustom( "Acc Name :" + widget.depositorname.toString(), 1, 0);
          bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
          bluetooth.printCustom("Transaction Date :" + widget.transactiondate.toString(), 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("Customer Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printCustom("please come back again", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom("........customer copy.......", 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
        }

        if (widget.transWhat == "EOD") {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prr = BaseClass.progreDialog(context);
          prr.show();
          var url = Uri.parse(Network.URL + "GetEODReport");
          Map<String, String> header = {
            "Content-Type": "application/json; charset=utf-8"
          };

          Map reportBlock = {
            "authorization_credentials": {
              "api_key": "12345",
              "token": Network.appToken
            },
            "corporateno": prefs.getString("corporateno") ?? "",
            "requestdate": BaseClass.getFormattedDate(),
            "agentid": prefs.getString("agtcode") ?? "",
            "terminalid": prefs.getString("imei") ?? "",
            "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
            "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
          };
          String body = json.encode(reportBlock);
          Response response = await post(url, headers: header, body: body);
          var responseData = jsonDecode(response.body);
          if (responseData['is_successful']) {
            await prr.hide();
            eodRefNo.clear();
            eodAmount.clear();
            eodTransactionType.clear();

            for (int i = 0; i < responseData["accounts"].length; i++) {
              eodRefNo.add(responseData["accounts"][i]["referenceno"]);
              eodAmount.add(responseData["accounts"][i]["amount"].toString());
              eodTransactionType
                  .add(responseData["accounts"][i]["transactiontype"]);
            }
          } else {
            BaseClass.alertDialog(
                context, "Failed", responseData['error'], "Close","assets/images/error.jpg");
          }

          bluetooth.printNewLine();
          bluetooth.printCustom("............................", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.sacconame, 1, 1);
          bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
          bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
          bluetooth.printCustom(widget.title, 1, 1);
          bluetooth.printNewLine();

          bluetooth.printNewLine();
          bluetooth.printLeftRight("RefNo   Amount", "Transaction",1);
          for (int i = 0; i < eodRefNo.length; i++) {
            bluetooth.printLeftRight(eodRefNo[i]+"  "+eodAmount[i], eodTransactionType[i],1);
            bluetooth.printNewLine();
          }
          bluetooth.printNewLine();
          bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
          bluetooth.printNewLine();
          bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          bluetooth.printCustom(widget.moto, 1, 1);
          bluetooth.printNewLine();
          bluetooth.printNewLine();
          BaseClass.disableBluetooth();
          Navigator.pushNamed(context, "dashboard");
        }
      }
    });
  }

  void AgentCopy() async {
    bluetooth.isConnected.then((isConnected) async {
      if (isConnected) {
          if (widget.transWhat == "D") {
            bluetooth.printNewLine();
            bluetooth.printCustom("............................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 0);
            bluetooth.printCustom(widget.title, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
            bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
            bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
            bluetooth.printCustom("Description :" + widget.decription.toString(), 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Deposited By :" + widget.depbyy, 1, 0);
            bluetooth.printCustom("Customer Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("......agent copy......", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();

          }
          if (widget.transWhat == "W") {
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
            bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.title, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
            bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
            bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
            bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);

            bluetooth.printNewLine();
            bluetooth.printCustom("ID :" + "........................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Customer Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("......agent copy......", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();

          }

          if (widget.transWhat == "LR") {
            bluetooth.printNewLine();
            bluetooth.printCustom(".............................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
            bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.title, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
            bluetooth.printCustom("Phone Number :" + widget.depositorphone, 1, 0);
            bluetooth.printCustom("Loan type :" + widget.loantype, 1, 0);
            bluetooth.printCustom("Loan number :" + widget.memberno, 1, 0);
            bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
            bluetooth.printCustom("Paid by :" + widget.depbyy.toString(), 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Customer Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("......agent copy......", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();

            bluetooth.printNewLine();
          }
          if (widget.transWhat == "SD") {
            bluetooth.printNewLine();
            bluetooth.printCustom("..........................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Receipt No :" + widget.refno, 1, 1);
            bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.title, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("Name :" + widget.depositorname, 1, 0);
            bluetooth.printCustom("Acc Name :" + widget.accountname, 1, 0);
            bluetooth.printCustom("Acc number :" + widget.memberno, 1, 0);
            bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("Customer Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("......agent copy......", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();

          }

          if (widget.transWhat == "LT") {
            bluetooth.printNewLine();
            bluetooth.printCustom("..........................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Reprint Date :" + BaseClass.getFormattedDate(), 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("RECEIPT REPRINT", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printCustom("TransactionType :" + widget.title, 1, 0);
            bluetooth.printCustom("Copy Receipt number :" + widget.refno, 1, 0);
            bluetooth.printCustom("Description :" + widget.decription, 1, 0);
            bluetooth.printCustom( "Acc Number :" + widget.memberno.toString(), 1, 0);
            //bluetooth.printCustom( "Acc Name :" + widget.depositorname.toString(), 1, 0);
            bluetooth.printCustom("Amount :" + widget.amt.toString(), 1, 0);
            bluetooth.printCustom("Transaction Date :" + widget.transactiondate.toString(), 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("Customer Signature :" + "............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + "............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom("Served by :" + widget.agentname, 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom("......agent copy......", 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
          }
          /*if (widget.transWhat == "EOD") {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prr = BaseClass.progreDialog(context);
            prr.show();
            var url = Uri.parse(Network.URL + "GetEODReport");
            Map<String, String> header = {
              "Content-Type": "application/json; charset=utf-8"
            };

            Map reportBlock = {
              "authorization_credentials": {
                "api_key": "12345",
                "token": Network.appToken
              },
              "corporateno": prefs.getString("corporateno") ?? "",
              "requestdate": BaseClass.getFormattedDate(),
              "agentid": prefs.getString("agtcode") ?? "",
              "terminalid": prefs.getString("imei") ?? "",
              "longitude":  double.parse(prefs.getString("longitude") ?? ""),
              "latitude":  double.parse(prefs.getString("latitude") ?? "")
            };
            String body = json.encode(reportBlock);
            Response response = await post(url, headers: header, body: body);
            var responseData = jsonDecode(response.body);
            if (responseData['is_successful']) {
              await prr.hide();
              eodRefNo.clear();
              eodAmount.clear();
              eodTransactionType.clear();

              for (int i = 0; i < responseData["accounts"].length; i++) {
                eodRefNo.add(responseData["accounts"][i]["referenceno"]);
                eodAmount.add(responseData["accounts"][i]["amount"].toString());
                eodTransactionType
                    .add(responseData["accounts"][i]["transactiontype"]);
              }
            } else {
              BaseClass.alertDialog(
                  context, "Failed", responseData['error'], "Close","assets/images/error.jpg");
            }

            bluetooth.printNewLine();
            bluetooth.printCustom("............................", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.sacconame, 1, 1);
            bluetooth.printCustom("Terminal :" + widget.deviceid, 1, 1);
            bluetooth.printCustom("Date :" + BaseClass.getFormattedDate(), 1, 1);
            bluetooth.printCustom(widget.title, 1, 1);
            bluetooth.printNewLine();
            
            bluetooth.printNewLine();
            for (int i = 0; i < eodRefNo.length; i++) {
              bluetooth.printCustom(eodRefNo[i], 1, 0);
              bluetooth.printCustom(eodAmount[i], 1, 1);
              bluetooth.printCustom(eodTransactionType[i], 2, 0);
              bluetooth.printNewLine();
            }
            bluetooth.printNewLine();
            bluetooth.printCustom("Agent Signature :" + ".............", 1, 0);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
            bluetooth.printCustom(widget.moto, 1, 1);
            bluetooth.printNewLine();
            bluetooth.printNewLine();
          }*/
          BaseClass.disableBluetooth();
          Navigator.pushNamed(context, "dashboard");
        }//todo re initialize connection
    });
  }

  Future show(
      String message, {
        Duration duration: const Duration(seconds: 10),
      }) async {
    await new Future.delayed(new Duration(seconds: 10));

    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text(
          message,
          style: new TextStyle(
            color: Colors.white,
          ),
        ),
        duration: duration,
      ),
    );
  }
}

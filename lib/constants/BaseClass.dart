import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Network.dart';
import 'Utils.dart';

class BaseClass {

  static BluetoothState _bluetoothState = BluetoothState.UNKNOWN;
  static FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;
  static List<BluetoothDevice> _devicesList = [];

  static String message;
  static alertDialog(BuildContext context, String myTitle, String myContent,
      String myAction, String image) {
    TextEditingController txtpin = new TextEditingController();
    Size size = MediaQuery
        .of(context)
        .size;
    Dialog confirm = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context, myTitle, myContent, myAction,image),
    );
    showDialog(barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => confirm);
  }

  static contentBox(context, title, content, action, img) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: kDefaultPadding,
              top: kDefaultPadding + kDefaultPadding,
              right: kDefaultPadding,
              bottom: kDefaultPadding),
          margin: EdgeInsets.only(top: kDefaultPadding),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kDefaultPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                content,
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 22,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                    onPressed: () {
                      if (action == "Ok") {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'dashboard', (route) => false);
                      } else if (action == "Logout") {
                        Navigator.of(context)
                            .pushNamedAndRemoveUntil('login', (route) => false);
                      } else if (action == "Close") {
                        Navigator.of(context).pop();
                      } else if (action == "Continue") {
                        Navigator.of(context).pop();
                      }else if (action == "Verify") {
                        Navigator.of(context).pop();
                      }else if(action=="Exit"){
                        SystemNavigator.pop();
                      }
                    },
                    child: Text(
                      action,
                      style: TextStyle(fontSize: 18),
                    )),
              ),
            ],
          ),
        ),
        Positioned(
          left: kDefaultPadding,
          right: kDefaultPadding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: kDefaultPadding,
            child: ClipRRect(
                borderRadius:
                BorderRadius.all(Radius.circular(kDefaultPadding)),
                child: Image.asset(img)),
          ),
        ),
      ],
    );
  }

  static toast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static fetchMemberDetails(String id, int code, BuildContext context) async {
    var url = Uri.parse(Network.URL + "GetMembername");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberDetailsBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporateno": prefs.getString("corporateno") ?? "",
      "accountidentifier": id,
      "accountcode": code, //2 for account number,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
    };

    String body = json.encode(memberDetailsBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        Map responseMap = {
          "memberName": responseData['customer_name'],
          "memberPhone": responseData['phoneno'],
        };

        return responseMap;
      } else {
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
        return null;
      }
    } else {
      alertDialog(context, "Failed", "Please try again", "Ok","assets/images/error.jpg");
      return null;
    }
  }

  static String getFormattedDate() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('MM/dd/yyyy h:mm a').format(now);
    return formattedDate;
  }
  static double getLatandLong() {
    double latlong = 0.0;
    return latlong;
  }

  static sharePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   return prefs;
  }
  static Future<dynamic> requestMemberAccounts(BuildContext context,String id,String transactionType) async {
    var url = Uri.parse(Network.URL + "GetAllMemberSaccoDetails");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAccountsBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },

      "corporateno": prefs.getString("corporateno") ?? "",
      "accountidentifier": id,
      "accountidentifiercode": "0", //2 for account number
      "transactiontype": transactionType,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": getFormattedDate()
    };

    String body = json.encode(memberAccountsBlock);
    print("accountdetails---->"+body.toString());

    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        return responseData;
      } else {
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
        return responseData['error'];
      }
    }
  }

  static progreDialog(BuildContext context){
    ProgressDialog pr = ProgressDialog(context);
    pr = ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        message: 'Please wait......',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    return pr;
  }

  static requestActiveLoans(BuildContext context,String id) async {
    var url = Uri.parse(Network.URL + "GetMemberActiveLoans");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map loanProductsBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporate_no": prefs.getString("corporateno") ?? "",
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude":  BaseClass.getLatandLong(),//double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(loanProductsBlock);
    Response response = await post(url, headers: header, body: body);

    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        return responseData;
      } else {
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
      }
    }
  }

  static Future<dynamic> requestSyncDropdown(BuildContext context,String code) async {
    var url = Uri.parse(Network.URL + "GetSaccoBranch");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAccountsBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },

      "corporateno": prefs.getString("corporateno") ?? "",
      "code": code,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
    };

    String body = json.encode(memberAccountsBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        return responseData;
      } else {
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
      }
    }
  }

  static Future<void> enableBluetooth() async {
    _bluetoothState = await FlutterBluetoothSerial.instance.state;
    if (_bluetoothState == BluetoothState.STATE_OFF) {
      await FlutterBluetoothSerial.instance.requestEnable();
      await getPairedDevices();
      return true;
    } else {
      await getPairedDevices();
    }
    return false;
  }

  static Future<void> getPairedDevices() async {
    List<BluetoothDevice> devices = [];

    try {
      devices = await _bluetooth.getBondedDevices();
    } on PlatformException {
      print("Error");
    }
  }


  static Future<void> disableBluetooth() async {
    _bluetoothState = await FlutterBluetoothSerial.instance.state;
    if (_bluetoothState == BluetoothState.STATE_ON) {
      await FlutterBluetoothSerial.instance.requestDisable();
      return true;
    } else {
      await getPairedDevices();
    }
    return false;
  }

  static getPicture(String identifier, BuildContext context) async {
    var url = Uri.parse(Network.URL + "PhotoSignature");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAccountsBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
    "identifier": identifier,
    "identifiercode": "1",
    "agentid": prefs.getString("agtcode") ?? "",
    "terminalid": prefs.getString("imei") ?? "",

    };

    String body = json.encode(memberAccountsBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        return responseData;
      } else {
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
      }
    }
  }

  static generatePINAndSend(BuildContext context, String phoneno) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var randomPIN = new Random().nextInt(9000) + 1000;

    message = "Your verification code is " +
        randomPIN.toString() +
        ". Enter this code to proceed.\nThank you.";

    await prefs.setString('otp', randomPIN.toString());

    var url = Uri.parse(Network.URL + "SendTextMessage");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map otpBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporateno": prefs.getString("corporateno") ?? "",
      "telephone": phoneno,
      "text": message,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
    };

    String body = json.encode(otpBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        prr.hide();
        return responseData;
      } else {
        prr.hide();
        alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
      }
    } else {
      prr.hide();
      alertDialog(context, "Failed", "Connection problem, please try again later...", "Ok","assets/images/error.jpg");
    }
  }
}
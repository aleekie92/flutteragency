import 'dart:ui';

import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

const colorPrimary = Color(0xFFd32f2f);
const kPrimaryColor = Color(0xFFd32f2f);
/*const colorPrimary = Color(0xFF003a17);
const kPrimaryColor = Color(0xFF003a17);*/

const kPrimaryColor1 = Color(0xFF2196F3);
const kPrimaryColor2 = Color(0xFF191970);
const kTextColor = Color(0xFF3C4046);
const kBackgroundColor = Color(0xFFF9F8FD);
const double kDefaultPadding = 20.0;

sharedPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs;
}

String citizenship = "citizenship";
String memberemployer = "memberemployer";
String memberemployercode = "memberemployercode";
String membermaritalstatus = "membermaritalstatus";
String membergender = "membergender";
String membertitle = "membertitle";

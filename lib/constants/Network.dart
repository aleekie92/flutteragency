import 'dart:io';

import 'package:agency_banking/constants/BaseClass.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Utils.dart';

class Network {
  static String URL="http://172.18.6.8:35088/api/AgencyBanking/"; //new environment - inside ke
  //static String URL="https://197.248.9.253:2020/agency/api/AgencyBanking/"; //agency-copmis - outside ke

  /*flutter build apk --obfuscate --split-debug-info=F:\files\CORETEC\documents\agencyAppObfucateFile\debug*/

  static String appToken;
  static TextEditingController txtpin = new TextEditingController();
  static Dialog authdialog;
  static generateToken(String ytak, BuildContext context) async {
    var url = Uri.parse(URL+ytak);
    String takta = "appsecret";
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };
    Map authBody = {"api_key": "12345", "app_secret": takta};
    String body = json.encode(authBody);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        appToken = responseData['token'];
      }
    }else{
      BaseClass.alertDialog(context, "Failed", "Check internet connection and try again", "Ok","assets/images/error.jpg");
    }
  }

   static authDialog(BuildContext context, String id) {
    Size size = MediaQuery .of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
      authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 180,
          width: size.width*3,
          child: Column(
            children: <Widget>[
              Text("Enter your pin to proceed.".toUpperCase(),
                style: TextStyle(color: Colors.blue),),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtpin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Pin",
                    border:
                    OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),

              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: colorPrimary,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil('dashboard', (route) => false);
                      }
                  ),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        if(txtpin.text.isEmpty){
                          BaseClass.toast("Enter pin");///await authentit
                        }else {
                          memberAuthRequest(context, id);
                        }
                      } //submit,
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(barrierDismissible: false,context: context, builder: (BuildContext context) => authdialog);
  }

  static memberAuthRequest(BuildContext context, String id) async {
    var url = Uri.parse(Network.URL + "AuthenticateCustomerLogin");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAuthBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "pin": txtpin.text.toString(),
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(memberAuthBlock);

    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        Navigator.of(context).pop();
        return responseData;
      } else {
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
        return null;
      }
    } else {
      BaseClass.alertDialog(context, "Failed", "Please try again", "Ok","assets/images/error.jpg");
      return null;
    }
  }
}

class DevHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

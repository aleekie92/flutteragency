import 'dart:convert';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'file:///F:/projects/android/agency_banking/lib/constants/printjob.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoanRepayment extends StatefulWidget {
  _LoanRepayment createState() => _LoanRepayment();
}

class _LoanRepayment extends State<LoanRepayment> {
  TextEditingController txtID = new TextEditingController();
  TextEditingController txtAmount = new TextEditingController();
  TextEditingController txtPaidBy = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool visible = false;

  String nametextholder = '**********';
  String phonetextholder = '**********';

  List<String> LoanProductsNoList = [];

  @override
  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> dataList) {
    List<DropdownMenuItem<String>> accountItems = new List();
    if (dataList.isNotEmpty) {
      for (String acc in dataList) {
        accountItems
            .add(new DropdownMenuItem(value: acc, child: new Text(acc)));
      }
      return accountItems;
    } else {
      print("seeing null");
    }
  }

  String _product;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Loan Repayment',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  memberDetails,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.number,
                maxLength: 10,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  setState(() {
                    visible = true;
                  });
                  searchFuntionality();
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      Text(
        "Select Loan product",
        style: TextStyle(
            color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18),
      ),
      loadLoanProductsData,
      new ListTile(
        leading: const Icon(Icons.monetization_on_outlined),
        title: new TextField(
          controller: txtAmount,
          keyboardType: TextInputType.number,
          decoration: new InputDecoration(
            hintText: "Enter amount",
          ),
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.perm_identity),
        title: new TextField(
          keyboardType: TextInputType.text,
          controller: txtPaidBy,
          textCapitalization: TextCapitalization.characters,
          decoration: new InputDecoration(
            hintText: "Paid by",
          ),
        ),
      ),
      SizedBox(
        height: 15.0,
      ),
      new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: colorPrimary,
        child: MaterialButton(
          minWidth: 150, //MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String splitedmemberphonee='$phonetextholder'.substring('$phonetextholder'.length - 9);
            if (txtID.text.isEmpty ||
                txtAmount.text.isEmpty ||
                nametextholder == "***********" ||
                nametextholder == "***********") {
              BaseClass.toast("please Key in the missing fields.");
            } else if (_product == null) {
              BaseClass.toast("please select account number.");
            }  else {
              if (prefs.getString("agtphone") == splitedmemberphonee) {
                BaseClass.toast("self loan repayment disabled.");
              } else {
                BaseClass.enableBluetooth();
                PostLoanRepaymentRequest(
                  txtID.text.toString(),
                  _product,
                  int.parse(txtAmount.text.toString()),
                  '$phonetextholder',
                  '$nametextholder',);
              }
            }
          },
          child: Text("Submit".toUpperCase(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    ]);
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: visible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality() async {
    var response =
        await BaseClass.fetchMemberDetails(txtID.text.toString(), 0, context);
    try {
      if (response != null) {
        if (response['memberName'].toString() == "" ||
            response['memberPhone'].toString() == "") {
          BaseClass.toast("Confirm ID number and try again...");
          setState(() {
            visible = false;
          });
        } else {
          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            visible = false;
          });
        }
      } else {
        setState(() {
          visible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        visible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
            new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor1),
            ),
            Spacer(),
            showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }

  PostLoanRepaymentRequest(String id, final String lnNo, final int amount,
      final String depositorsPhoneNo, final String depositorsName) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String terminalid = prefs.getString("imei") ?? "";
    String agentnamee = prefs.getString("accname") ?? "";
    String saccomotto = prefs.getString("saccomoto") ?? "";
    var url = Uri.parse(Network.URL + "LoanRepayment");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map withdrawalBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporate_no": prefs.getString("corporateno") ?? "",
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "loan_no": lnNo.split(':').sublist(1).join(':').trim(),
      "repayment_amount": amount,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };
    String body = json.encode(withdrawalBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      prr.hide();
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        String rNo = responseData['receiptno'];
        String transType = responseData['transactiontype'];
        String sName = responseData['sacconame'];
        String tDate = responseData['transactiondate'];

        confirmdialog(context,lnNo,amount,depositorsPhoneNo,depositorsName,terminalid,agentnamee,rNo,sName,tDate,saccomotto);

        prr.hide();
      } else {
        prr.hide();
        BaseClass.alertDialog(context, "Error!", responseData['error'], "Ok","assets/images/error.jpg");
      }
    } else {
      prr.hide();
      BaseClass.alertDialog(
          context, "Error!", "Failed, please try again later", "Ok","assets/images/error.jpg");
    }
  }

  get loadLoanProductsData {
    String id = txtID.text.toString();
    if (id.isNotEmpty) {
      return FutureBuilder(
          future: BaseClass.requestActiveLoans(context, id),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );
            } else {

              try {
                LoanProductsNoList.clear();
                for (int i = 0; i < snapshot.data["active_loans"].length; i++) {
                  LoanProductsNoList.add(snapshot.data["active_loans"][i]
                          ["loantypename"] + " : " +snapshot.data["active_loans"][i]["loan_no"]);
                }

                return new DropdownButton(
                  hint: Text("Select Loan product"),
                  value: _product,
                  items: getDropDownMenuItems(LoanProductsNoList),
                  onChanged: productsChangedDropDownItem,
                );
              } catch (e) {
                print("error found $e");
                return Text(snapshot.data.toString());
              }
            }
          });
    } else {
      return new DropdownButton(
        value: _product,
        items: getDropDownMenuItems(LoanProductsNoList),
        onChanged: productsChangedDropDownItem,
      );
    }
  }

  void productsChangedDropDownItem(String selectedAccount) {
    setState(() {
      _product = selectedAccount;
    });
  }


  confirmdialog(BuildContext context, String lnNo, int amount, String depositorsPhoneNo, String depositorsName, String terminalid,
      String agentnamee, String rNo,String sName, String tDate,String saccomotto) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog successdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child:
      Stack(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
              height: 220,
              width: size.width * 3,
              child: Column(
                children: <Widget>[

                  SizedBox(height: 45.0),
                  Text(
                    "Loan repayment submitted successfully.",
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 15.0),
                  Text(
                    "Would you wish to print receipt?".toUpperCase(),
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 30.0),
                  new Row(
                    children: <Widget>[
                      RaisedButton(
                          child: Text(
                            "No",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: kPrimaryColor,
                          onPressed: () async {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                'dashboard', (route) => false);
                          }
                        //submit,
                      ),

                      Spacer(),

                      RaisedButton(
                          child: Text(
                            "Yes",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PrintDemo(
                                      transWhat: "LR",
                                      memberno: lnNo.split(':').sublist(1).join(':').trim(),
                                      amt: amount,
                                      title: "LOAN REPAYMENT",
                                      depositorphone: depositorsPhoneNo,
                                      depositorname: depositorsName,
                                      deviceid: terminalid,
                                      agentname: agentnamee,
                                      refno: rNo,
                                      sacconame: sName,
                                      idNo: "",
                                      transactiondate: tDate,
                                      decription: "",
                                      depbyy: txtPaidBy.text.toString(),
                                      moto: saccomotto,
                                      loantype: lnNo.split(':')[0].trim()
                                  ),
                                ));
                          }
                        //submit,
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  )
                ],
              )
          ),
          Positioned(
            left: kDefaultPadding,
            right: kDefaultPadding,
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: kDefaultPadding,
              child: ClipRRect(
                  borderRadius:
                  BorderRadius.all(Radius.circular(kDefaultPadding)),
                  child: Image.asset("assets/images/tick.jpg")),
            ),
          ),
        ],
      ),


    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => successdialog);
  }
}

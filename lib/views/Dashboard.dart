import 'dart:convert';

import 'package:agency_banking/components/Body.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> with WidgetsBindingObserver{
  String sacconanetxtholder="CORE-CASH SOLUTION";
  SharedPreferences prefs;
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    requestAgentAccount();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      requestAgentAccount();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async {
          Fluttertoast.showToast(
            msg: 'Click on logout icon to LOGOUT',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
          );
          return false;
        },
   child: Scaffold(
    appBar: buildAppBar(),
    body: Body(),
    ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 0,
      title: Row(
        children: [
            new Text(
               '$sacconanetxtholder',
              style: TextStyle(
                fontSize: 12,
              ),
            ),
            Spacer(),
          CircleAvatar(
              backgroundColor: Colors.white,
              child: IconButton(
                icon: const Icon(Icons.logout),
                onPressed: () async {
                  BaseClass.disableBluetooth();
                  BaseClass.alertDialog(context, "Alert!",
                      "Confirm you want to logout", "Logout","assets/images/error.jpg");
                },
              )),

        ],
      ),
    );
  }

  saveAgentsAccountsInfo(var responseData) async {
    prefs = await SharedPreferences.getInstance();
    await prefs.setString('corporateno', responseData['corporateno']);
    await prefs.setString('accname', responseData['agentdetails']['accountname']);//check badae
    await prefs.setString('sacconame', responseData['sacconame']);
    await prefs.setString('saccomoto', responseData['saccomotto']);
    await prefs.setString('businessname', responseData['agentdetails']['proposedname']);
    await prefs.setString('agentaccount', responseData['agentdetails']['agentaccno']);
    await prefs.setString('bussinesslocation', responseData['agentdetails']['location']);
    await prefs.setDouble('agtaccbalance',  responseData['agentdetails']['accountbalance']);
    await prefs.setDouble('agentaccountcommissionbalance', responseData['agentdetails']['accountcommissionbalance']);
    getSaccoName();
  }

  Future<String> getAgentCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String agtCode= prefs.getString("agtcode") ?? "";
    return agtCode;
  }

  Future<void> getSaccoName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     sacconanetxtholder= prefs.getString("sacconame") ?? "";
  }

  requestAgentAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse(Network.URL + "GetAgentAccountInfo");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map agentInfoBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "agentid": await getAgentCode(),
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate(),
    };

    String body = json.encode(agentInfoBlock);
    Response response = await post(url, headers: header, body: body);

    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
       saveAgentsAccountsInfo(responseData);
      } else {
        //BaseClass.alertDialog(context, "Failed", responseData['error'], "Close","assets/images/error.jpg");
      }
    }
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountActivation extends StatefulWidget {
  _AccountActivation createState() => _AccountActivation();
}

class _AccountActivation extends State<AccountActivation> {
  TextEditingController txtID = new TextEditingController();
  TextEditingController txtOtp = new TextEditingController();
  TextEditingController txtPin = new TextEditingController();
  TextEditingController txtConfirmPin = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool progressvisible = false;
  bool verycodevisible = false;
  bool pincodevisible = false;
  bool repincodevisible = false;
  bool submitprogressbar = false;
  String nametextholder = "***********";
  String phonetextholder = "***********";
  String randomCode, plainCode, refConfirmation;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Account Activation',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Column(
              children: <Widget>[
                memberDetails,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: progressvisible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality() async {
    var response = await BaseClass.fetchMemberDetails(txtID.text.toString(),0,context);
    try {
      if (response != null) {
        if (response['memberName'].toString() == "" ||
            response['memberPhone'].toString() == "") {
          BaseClass.toast("Confirm ID number and try again...");
          setState(() {
            progressvisible = false;
          });
        } else {

          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            verycodevisible = true;
          });
          var responsee = await BaseClass.generatePINAndSend(context, '$phonetextholder');
          if (responsee != null) {
            BaseClass.alertDialog(
                context, "Success", "OTP has been send to you", "Verify","assets/images/tick.jpg");
            //buttonText = "Submit";
            setState(() {
              pincodevisible = true;
              repincodevisible = true;
              progressvisible = false;
            });
          } else {
            BaseClass.toast("Failed, please try again....");
          }
        }
      } else {
        setState(() {
          progressvisible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        progressvisible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
            new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(fontSize: 14,fontWeight: FontWeight.bold,color: kPrimaryColor1),
            ),
            Spacer(),
            showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                maxLength: 10,
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                //SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  progressvisible = true;
                  setState(() {});
                  searchFuntionality();
                }
              },

              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: verycodevisible,
        child: ListTile(
          leading: const Icon(Icons.code),
          title: new TextField(
            controller: txtOtp,
            keyboardType: TextInputType.number,
            maxLength: 4,
            decoration: new InputDecoration(
              hintText: "Enter Verification Code",
            ),
          ),
        ),
      ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: pincodevisible,
        child: ListTile(
          leading: const Icon(Icons.code),
          title: new TextField(
            keyboardType: TextInputType.number,
            controller: txtPin,
            obscureText: true,
            maxLength: 4,
            decoration: new InputDecoration(
              hintText: "Enter Pin",
            ),
          ),
        ),
      ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: repincodevisible,
        child: ListTile(
          leading: const Icon(Icons.code),
          title: new TextField(
            controller: txtConfirmPin,
            keyboardType: TextInputType.number,
            obscureText: true,
            maxLength: 4,
            decoration: new InputDecoration(
              hintText: "Re-Enter Pin",
            ),
          ),
        ),
      ),
      SizedBox(
        height: 15.0,
      ),


      new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: colorPrimary,
        child: MaterialButton(
          minWidth: 150, //MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            if ('$nametextholder' == "***********") {
              BaseClass.toast(
                  "Enter ID number and search to fetch account details!");
            } else {
                String confirmation = prefs.getString("otp") ?? "";
                if (confirmation.length >= 4) {
                  if (confirmation == txtOtp.text.toString()) {
                    if(txtPin.text.toString() != txtConfirmPin.text.toString()){
                      BaseClass.toast("Entered pin do not match");
                    }else {
                      setState(() {
                        submitprogressbar = true;
                      });
                      submitActivation();
                    }
                  } else {
                    BaseClass.toast(
                        "Invalid verification code! Please try again");
                  }
              }
            }
          },

          child: Text("SUBMIT".toUpperCase(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)
          ),
        ),
      ),


      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: submitprogressbar,
        child: CircularProgressIndicator(
          backgroundColor: colorPrimary,
          strokeWidth: 3,
        ),
      ),
    ]);
    /*   }
  });*/
  }

  submitActivation() async {
    var url = Uri.parse(Network.URL + "MemberActivation");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map activationBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporate_no": prefs.getString("corporateno") ?? "",
      "accountidentifier": txtID.text.toString(), //txtOtp.text.toString(),
      "newpin": txtPin.text.toString(),
      "confirmpin": txtConfirmPin.text.toString(),
      "accountidentifiercode": 0,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate(),
      "phonenumber": '$phonetextholder',
    };

    String body = json.encode(activationBlock);
    setState(() {
      submitprogressbar = true;
    });
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      setState(() {
        submitprogressbar = false;
      });
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        setState(() {
          submitprogressbar = false;
        });
        BaseClass.alertDialog(
            context,
            "Success",
            "Member activation submited successfully. enjoy the agency banking services. ",
            "Ok","assets/images/tick.jpg");
      } else {
        setState(() {
          submitprogressbar = false;
        });
        BaseClass.alertDialog(
            context, "Error!", "Failed, please try again later", "Ok","assets/images/error.jpg");
      }
    }
  }
}

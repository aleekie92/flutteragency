import 'dart:convert';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
class BalanceInquiry extends StatefulWidget {
  _balance_inquiry createState() => _balance_inquiry();
}

class _balance_inquiry extends State<BalanceInquiry> {
  TextEditingController txtID = new TextEditingController();
  static TextEditingController txtpin = new TextEditingController();
  TextEditingController txtOtp = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool visible = false;
  bool submitprogressbar = false;
  bool accountsprogress = true;
  String nametextholder = "***********";
  String phonetextholder = "***********";

  List<String> accountNoList = [];
  List<String> phoneNoList = [];

  @override
  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> dataList) {
    List<DropdownMenuItem<String>> accountItems = new List();
    if (dataList.isNotEmpty) {
      for (String acc in dataList) {
        // here we are creating the drop down menu items, you can customize the item right here
        // but I'll just use a simple text for this
        accountItems
            .add(new DropdownMenuItem(value: acc, child: new Text(acc)));
      }
      return accountItems;
    } else {
      print("seeing null");
    }
  }

  String _account;

//dropdown items ends here

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Balance Inquiry',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  memberDetails,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.number,
                maxLength: 10,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  visible = true;
                  setState(() {});
                  searchFuntionality();
                  accountNoList.clear();
                  phoneNoList.clear();
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      new ListTile(
        title: Padding(
          padding: const EdgeInsets.only(
              left: kDefaultPadding + 5, right: kDefaultPadding + 5),
          child: Row(
            children: [
              CircleAvatar(
                radius: 40,
                backgroundImage: AssetImage("assets/images/user.jpg"),
              ),
              Spacer(),
              CircleAvatar(
                radius: 40,
                backgroundImage: AssetImage("assets/images/signature.png"),
              )
            ],
          ),
        ),
      ),
      Text(
        "Select account",
        style: TextStyle(
            color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 16),
      ),
      loadDataAccounts,
      SizedBox(
        height: 15.0,
      ),
      new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: colorPrimary,
        child: MaterialButton(
          minWidth: 150, //MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            if (txtID.text.isEmpty ||
                nametextholder == "***********" ||
                nametextholder == "***********") {
              BaseClass.toast("please Key in the missing fields.");
            } else {
              if (nametextholder == "***********" ||
                  phonetextholder == "***********") {
                BaseClass.toast("please Key in the missing fields.");
              } else if(txtID.text.isEmpty){
                BaseClass.toast("please key in ID number and search account.");
              }else if (_account == null) {
                BaseClass.toast("please select account number.");
              } else {
                authDialog(context, txtID.text.toString());
              }
            }
          },
          child: Text("Submit".toUpperCase(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: submitprogressbar,
        child: CircularProgressIndicator(
          backgroundColor: colorPrimary,
          strokeWidth: 3,
        ),
      ),
    ]);
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: visible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality() async {
    var response = await BaseClass.fetchMemberDetails(txtID.text.toString(), 0, context);
    try {
      if (response != null) {
        if (response['memberName'].toString() == "" ||
            response['memberPhone'].toString() == "") {
          BaseClass.toast("Confirm ID number and try again...");
          setState(() {
            visible = false;
          });
        } else {
          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            visible = false;
          });
        }
      } else {
        setState(() {
          visible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        visible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
            new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor1),
            ),
            Spacer(),
            //showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }


  get loadDataAccounts{
    String id=txtID.text.toString();
    if(id.isNotEmpty){
      return  FutureBuilder(
          future: BaseClass.requestMemberAccounts(context, id, "B"),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );
            } else {
              try {
                accountNoList.clear();
                phoneNoList.clear();
                for (int i = 0; i < snapshot.data["saccoaccounts"].length; i++) {

                  for (int j = 0; j <  snapshot .data["saccoaccounts"][i]["accountdetails"].length; j++) {
                    accountNoList.add(snapshot.data["saccoaccounts"][i]
                    ["accountdetails"][j]["accountno"] +
                        " - " +
                        snapshot.data["saccoaccounts"][i]["accountdetails"][j]
                        ["accounttype"]);
                    phoneNoList.add(snapshot.data["saccoaccounts"][i]
                    ["accountdetails"][j]["phonenumber"]);

                    print("GetSaccoAccounts ${snapshot.data["saccoaccounts"][i]
                    ["accountdetails"][j]["accountno"]}");
                  }
                }
                return new DropdownButton(
                  hint: Text("Select Account"),
                  value: _account,
                  items: getDropDownMenuItems(accountNoList),
                  onChanged: productsChangedDropDownItem,
                );
              } catch (e) {
                print("error found $e");
                return Text(snapshot.data.toString());
              }
            }
          }
      );
    }else{
      return new DropdownButton(
        value: _account,
        items: getDropDownMenuItems(accountNoList),
        onChanged: productsChangedDropDownItem,
      );
      BaseClass.toast("please enter ID and give it a second....");
    }
  }

  postBalanceInquiryRequest(String id, accountNo) async {
    ProgressDialog prr= BaseClass.progreDialog(context);
    prr.show();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String terminalid=prefs.getString("imei") ?? "";
    String agentnamee=prefs.getString("accname") ?? "";
    var url = Uri.parse(Network.URL + "GetBalanceEnquiry");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map withdrawalBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "accountidentifier": id, //txtID.text.toString(),
      "corporate_no": prefs.getString("corporateno") ?? "",
      "accountidentifiercode": "0",
      "accountno": accountNo.split('-')[0].trim(),
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };
    String body = json.encode(withdrawalBlock);

    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        String customerName=responseData['customername'];
        double amt=responseData['balance']['amount'];
        String  accountNumber=responseData['balance']['account'];
        String  receiptNumber=responseData['receiptno'];
        String transactionType=responseData['transactiontype'];
        String saccoName=responseData['sacconame'];
        String transactionDate=responseData['transactiondate'];
        sendSMS(customerName,amt ,accountNumber , agentnamee,receiptNumber ,transactionType,saccoName ,transactionDate);
        prr.hide();
      } else {

        prr.hide();
        BaseClass.alertDialog(
            context, "Error!", "Failed, please try again later", "Ok","assets/images/error.jpg");
      }
    }
  }

  sendSMS(String customerName, double amt,String accountNumber, String agentName, String receiptNumber, String transactionType,
      String saccoName, String transactionDate) async {
      String message ="Balance Enquiry\nHi "+customerName+ ", your total balance for account " +accountNumber+ " is " +amt.toString()+ ".\nYou were served by "+ agentName +". Thank you.";
     SharedPreferences prefs = await SharedPreferences.getInstance();
     var url = Uri.parse(Network.URL + "SendTextMessage");
     Map<String, String> header = {
       "Content-Type": "application/json; charset=utf-8"
     };

     Map otpBlock = {
       "authorization_credentials": {
         "api_key": "12345",
         "token": Network.appToken
       },
       "corporateno": prefs.getString("corporateno") ?? "",
       "telephone": '$phonetextholder',
       "text": message,
       "agentid": prefs.getString("agtcode") ?? "",
       "terminalid": prefs.getString("imei") ?? "",
       "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
       "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
     };

     String body = json.encode(otpBlock);
     Response response = await post(url, headers: header, body: body);
     if (response != null) {
       var responseData = jsonDecode(response.body);
       if (responseData['is_successful']) {
         BaseClass.alertDialog(
             context,
             "Success",
             "Balance Inquiry submited successfully, balance sent as message to the members phone. ",
             "Ok","assets/images/tick.jpg");

       } else {
         BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
       }
     } else {
       BaseClass.alertDialog(context, "Failed",
           "Connection problem, please try again later...", "Ok","assets/images/error.jpg");
     }
  }

  void productsChangedDropDownItem(String selectedAccount) {
    setState(() {
      _account = selectedAccount;
      print("selected click= $selectedAccount");
    });
  }

  authDialog(BuildContext context, String id) {
    Size size = MediaQuery .of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 220,
          width: size.width*3,
          child: Column(
            children: <Widget>[
              Text("Enter your initial activation pin to proceed.".toUpperCase(),
                style: TextStyle(color: Colors.blue),),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtpin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Pin",
                    border:
                    OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),

              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: colorPrimary,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil('dashboard', (route) => false);
                      }
                  ),

                  Spacer(),

                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        memberAuthRequest(context, id);
                      } //submit,
                  ),

                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(barrierDismissible: false,context: context, builder: (BuildContext context) => authdialog);
  }

  memberAuthRequest(BuildContext context, String id) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    var url = Uri.parse(Network.URL + "AuthenticateCustomerLogin");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAuthBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "pin": txtpin.text.toString(),
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),//  double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(memberAuthBlock);
    Response response = await post(url, headers: header, body: body);
    setState(() {
      submitprogressbar=true;
    });
    if (response != null) {
      var responseData = jsonDecode(response.body);
      setState(() {
        submitprogressbar=false;
      });
      if (responseData['is_successful']) {
        prr.hide();
        setState(() {
          submitprogressbar=false;
        });
        Navigator.of(context).pop();
        var responsee = await BaseClass.generatePINAndSend(
            context, '$phonetextholder');
        if (responsee != null) {
          if (txtpin.text.isEmpty) {
            BaseClass.toast("Enter pin");
          } else {
            BaseClass.toast(
                "OTP has been send to " + '$phonetextholder');
            oneTimePinDialog(context);
          }
        } else {
          BaseClass.toast("Failed, please try again....");
        }

        txtpin.clear();
      } else {
        setState(() {
          submitprogressbar=false;
        });
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
        txtpin.clear();
      }
    } else {
      setState(() {
        submitprogressbar=false;
      });
      BaseClass.alertDialog(context, "Failed", "Please try again", "Ok","assets/images/error.jpg");
      txtpin.clear();
    }
  }


  oneTimePinDialog(BuildContext context) async {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 200,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Enter verification code sent to"+'$phonetextholder'.toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtOtp,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter OTP",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'dashboard', (route) => false);
                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        if (txtOtp.text.isEmpty) {
                          BaseClass.toast("Enter OTP");
                        } else {
                          String confirmation = prefs.getString("otp") ?? "";
                          if (confirmation.length >= 4) {
                            if (confirmation == txtOtp.text.toString()) {
                              Navigator.of(context).pop();
                              postBalanceInquiryRequest(txtID.text.toString(), _account);
                              txtpin.clear();
                            } else {
                              BaseClass.toast(
                                  "Incorrect verification code! Please try again");
                            }
                          }
                        }
                      } //submit,
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }
}

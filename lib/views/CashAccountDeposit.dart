import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:custom_switch/custom_switch.dart';
import 'file:///F:/projects/android/agency_banking/lib/constants/printjob.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class CashAccountDeposit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CashAccountDeposit();
}

class _CashAccountDeposit extends State<CashAccountDeposit> {
  TextEditingController txtID = new TextEditingController();
  TextEditingController txtAmount = new TextEditingController();
  TextEditingController txtAccount = new TextEditingController();
  TextEditingController txtDescription = new TextEditingController(text: 'Agency Banking Deposit');
  TextEditingController txtName = new TextEditingController();
  TextEditingController txtPhone = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool visible = false;
  bool submitprogressbar = false;
  String nametextholder = "***********";
  String phonetextholder = "***********";

  List<String> accountNoList = [];
  List<String> phoneNoList = [];

  int _radioValue = 0;
  bool status = false;

  @override
  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> dataList) {
    List<DropdownMenuItem<String>> accountItems = new List();
    if (dataList.isNotEmpty) {
      for (String acc in dataList) {
        accountItems
            .add(new DropdownMenuItem(value: acc, child: new Text(acc)));
      }
      return accountItems;
    } else {
      print("seeing null");
    }
  }

  @override
  void dispose() {
    txtAccount.dispose();
    super.dispose();
  }
  @override
  void initState() {
    txtAccount.addListener((){
      txtAmount.clear();
      setState(() {

      });
    });

    txtAmount.addListener((){
      if(txtAccount.text.isEmpty){
        BaseClass.toast("Enter account number first");
        txtAmount.clear();
      }
      setState(() {

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Cash Deposit',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  memberDetailsByAccountNo,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  get memberDetailsByAccountNo {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.characters,
                maxLength: 19,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtAccount,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Account number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtAccount.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtAccount.text.length < 6) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  setState(() {
                    visible = true;
                  });
                  searchFuntionality(txtAccount.text.toString(), 2);
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      new ListTile(
        leading: const Icon(Icons.monetization_on_outlined),
        title: new TextField(
          keyboardType: TextInputType.number,
          controller: txtAmount,
          decoration: new InputDecoration(
            hintText: "Amount",
          ),
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.description),
        title: new TextField(
          keyboardType: TextInputType.text,
          enabled: true,
          controller: txtDescription,
          decoration: new InputDecoration(hintText: "Description"),
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.perm_identity),
        title: new TextField(
          keyboardType: TextInputType.text,
          textCapitalization: TextCapitalization.characters,
          controller: txtName,
          decoration: new InputDecoration(
            hintText: "Depositor Name",
          ),
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: new TextField(
          keyboardType: TextInputType.number,
          controller: txtPhone,
          maxLength: 10,
          decoration: new InputDecoration(
            hintText: "Depositor Phone Number",
          ),
        ),
      ),
      SizedBox(
        height: 15.0,
      ),
      new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: colorPrimary,
        child: MaterialButton(
          minWidth: 150, //MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () async {

            if (txtDescription.text.isEmpty || txtAmount.text.isEmpty ||  txtName.text.isEmpty || txtPhone.text.isEmpty)  {
              BaseClass.toast("please Key in the missing fields.");
            }else if(nametextholder == "***********" ||
                phonetextholder == "***********"){
              BaseClass.toast("please click on search to pull account details.");
            } else {
                BaseClass.enableBluetooth();
                PostDepositRequest(
                    txtAccount.text.toString(),
                    int.parse(txtAmount.text.toString()),
                    txtDescription.text.toString(),
                    txtPhone.text.toString(),
                    txtName.text.toString());
            }
          },
          child: Text("Submit".toUpperCase(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: submitprogressbar,
        child: CircularProgressIndicator(
          backgroundColor: colorPrimary,
          strokeWidth: 3,
        ),
      ),
    ]);
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: visible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality(String accNo, int code) async {
    var response = await BaseClass.fetchMemberDetails(accNo, code, context);
    try {
      if (response != null) {
        nametextholder = response['memberName'].toString();
        phonetextholder = response['memberPhone'].toString();
        setState(() {
          visible = false;
        });

      } else {
        setState(() {
          visible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        visible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      //showProgress(),
       new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
             new Text(
                  nametextholder.toUpperCase(),
                  overflow: TextOverflow.fade,
                  style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor1),
            ),
            Spacer(),
            showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }

  PostDepositRequest(
      final String memberAccno,
      final int amount,
      final String deposit_description,
      final String depositorsPhoneNo,
      final String depositorsName) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String terminalid = prefs.getString("imei") ?? "";
    String agentnamee = prefs.getString("accname") ?? "";
    String saccomotto = prefs.getString("saccomoto") ?? "";

    var url = Uri.parse(Network.URL + "FundsDeposits");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map depositBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporate_no": prefs.getString("corporateno") ?? "",
      "idno": txtID.text.toString(),
      "memberaccno": memberAccno,
      "depositorsname": depositorsName,
      "AccountType": "D",
      "description": deposit_description,
      "depositorsphoneno": depositorsPhoneNo,
      "amount": amount,
      "AgentId": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),//  double.parse(prefs.getString("longitude") ?? ""),
      "latitude":  BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(depositBlock);
    Response response = await post(url, headers: header, body: body);
    prr.show();
    if (response != null) {
      prr.show();
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        prr.hide();
        String rNo = responseData['receiptno'];
        String sName = responseData['sacconame'];
        String tDate = responseData['transactiondate'];

        confirmdialog(context,memberAccno,amount,depositorsPhoneNo,depositorsName,terminalid,agentnamee,rNo,sName,tDate,saccomotto);

      } else {
        prr.hide();
        BaseClass.alertDialog(context, "Failed!",
            "Error :" + responseData['error'].toString(), "Ok","assets/images/error.jpg");
      }
    } else {
      prr.hide();
      BaseClass.alertDialog(
          context, "Failed!", "Error :" + "Something went wrong", "Ok","assets/images/error.jpg");
    }
  }

  confirmdialog(BuildContext context, String memberAccno, int amount, String depositorsPhoneNo, String depositorsName, String terminalid,
      String agentnamee, String rNo,String sName, String tDate,String saccomotto) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog successdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child:
      Stack(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
              height: 220,
              width: size.width * 3,
              child: Column(
                children: <Widget>[

                  SizedBox(height: 45.0),
                  Text(
                    "Cash deposit submitted successfully.",
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 15.0),
                  Text(
                    "Would you wish to print receipt?".toUpperCase(),
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 30.0),
                  new Row(
                    children: <Widget>[
                      RaisedButton(
                          child: Text(
                            "No",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: kPrimaryColor,
                          onPressed: () async {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                'dashboard', (route) => false);
                          }
                        //submit,
                      ),

                      Spacer(),

                      RaisedButton(
                          child: Text(
                            "Yes",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PrintDemo (
                                      transWhat: "D",
                                      memberno: memberAccno,
                                      amt: amount,
                                      title: "AGENT DEPOSIT",
                                      depositorphone: depositorsPhoneNo,
                                      depositorname: depositorsName,
                                      deviceid: terminalid,
                                      agentname: agentnamee,
                                      refno: rNo,
                                      sacconame: sName,
                                      idNo: "",
                                      transactiondate: tDate,
                                      decription: txtDescription.text.toString(),
                                      depbyy: depositorsName,
                                      moto: saccomotto,
                                      accountname:nametextholder.toUpperCase()),
                                ));

                          }
                        //submit,
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  )
                ],
              )
          ),

          Positioned(
            left: kDefaultPadding,
            right: kDefaultPadding,
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: kDefaultPadding,
              child: ClipRRect(
                  borderRadius:
                  BorderRadius.all(Radius.circular(kDefaultPadding)),
                  child: Image.asset("assets/images/tick.jpg")),
            ),
          ),
        ],
      ),


    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => successdialog);
  }
}

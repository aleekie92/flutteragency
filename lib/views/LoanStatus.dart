import 'dart:convert';

import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LoanStatus extends StatefulWidget {
  _LoanStatus createState() => _LoanStatus();
}

class _LoanStatus extends State<LoanStatus> {
  TextEditingController txtID = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool visible = false;

  String nametextholder = '**********';
  String phonetextholder = '**********';

  List<String> LoanProductsNoList = [];
  List<String> LoanBal = [];

  static TextEditingController txtpin = new TextEditingController();
  TextEditingController txtOtp = new TextEditingController();
  bool submitprogressbar = false;

  @override
  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> dataList) {
    List<DropdownMenuItem<String>> accountItems = new List();
    if (dataList.isNotEmpty) {
      for (String acc in dataList) {
        accountItems.add(new DropdownMenuItem(value: acc, child: new Text(acc.split(':')[0].trim())));
      }
      return accountItems;
    } else {
      print("seeing null");
    }
  }

  String _product;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Loan Status',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  memberDetails,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.number,
                maxLength: 10,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  setState(() {
                    visible = true;
                  });
                  searchFuntionality();
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,

      loadLoanProductsData,
      SizedBox(
        height: 15.0,
      ),
    ]);
  }


  Future<void> searchFuntionality() async {
    var response =
    await BaseClass.fetchMemberDetails(txtID.text.toString(), 0, context);
    try {
      if (response != null) {
        if (response['memberName'].toString() == "" ||
            response['memberPhone'].toString() == "") {
          BaseClass.toast("Confirm ID number and try again...");
          setState(() {
            visible = false;
          });
        } else {
          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            visible = false;
          });
        }
      } else {
        setState(() {
          visible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        visible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
            new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor1),
            ),
            Spacer(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }

  get loadLoanProductsData {
    String id = txtID.text.toString();
    if (id.isNotEmpty) {
      return FutureBuilder(
          future: BaseClass.requestActiveLoans(context, id),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );
            } else {
              try {
                LoanProductsNoList.clear();
                LoanBal.clear();
                for (int i = 0; i < snapshot.data["active_loans"].length; i++) {
                  LoanProductsNoList.add(snapshot.data["active_loans"][i]
                  ["loantypename"]+ " : " + snapshot.data["active_loans"][i]["loanbalance"].toString());
                }
                /**/
                return new DropdownButton(
                  hint: Text("Select Loan product"),
                  value: _product,
                  items: getDropDownMenuItems(LoanProductsNoList),
                  onChanged: productsChangedDropDownItem,
                );
              } catch (e) {
                print("error found $e");
                return Text(snapshot.data.toString());
              }
            }
          });
    } else {
      return new DropdownButton(
        hint: Text("Select Loan product"),
        value: _product,
        items: getDropDownMenuItems(LoanProductsNoList),
        onChanged: productsChangedDropDownItem,
      );
    }
  }

  void productsChangedDropDownItem(String selectedAccount) {
    setState(() {
      _product = selectedAccount;
      authDialog(context, txtID.text.toString());
    });
  }





  authDialog(BuildContext context, String id) {
    Size size = MediaQuery .of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 220,
          width: size.width*3,
          child: Column(
            children: <Widget>[
              Text("Enter your initial activation pin to proceed.".toUpperCase(),
                style: TextStyle(color: Colors.blue),),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtpin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Pin",
                    border:
                    OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),

              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: colorPrimary,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil('dashboard', (route) => false);
                      }
                  ),

                  Spacer(),

                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        memberAuthRequest(context, id);

                      } //submit,
                  ),

                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(barrierDismissible: false,context: context, builder: (BuildContext context) => authdialog);
  }

  memberAuthRequest(BuildContext context, String id) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    var url = Uri.parse(Network.URL + "AuthenticateCustomerLogin");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAuthBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "pin": txtpin.text.toString(),
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),//  double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(memberAuthBlock);
    Response response = await post(url, headers: header, body: body);
    setState(() {
      submitprogressbar=true;
    });
    if (response != null) {
      var responseData = jsonDecode(response.body);
      setState(() {
        submitprogressbar=false;
      });
      if (responseData['is_successful']) {
        prr.hide();
        setState(() {
          submitprogressbar=false;
        });
        Navigator.of(context).pop();

        var responsee = await BaseClass.generatePINAndSend(
            context, '$phonetextholder');
        if (responsee != null) {
          if (txtpin.text.isEmpty) {
            BaseClass.toast("Enter pin");
          } else {
            BaseClass.toast(
                "OTP has been send to " + '$phonetextholder');
            oneTimePinDialog(context);
          }
        } else {
          BaseClass.toast("Failed, please try again....");
        }

        txtpin.clear();
      } else {
        setState(() {
          submitprogressbar=false;
        });
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
        txtpin.clear();
      }
    } else {
      setState(() {
        submitprogressbar=false;
      });
      BaseClass.alertDialog(context, "Failed", "Please try again", "Ok","assets/images/error.jpg");
      txtpin.clear();
    }
  }


  oneTimePinDialog(BuildContext context) async {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 200,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Enter verification code sent to"+'$phonetextholder'.toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtOtp,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter OTP",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'dashboard', (route) => false);
                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        if (txtOtp.text.isEmpty) {
                          BaseClass.toast("Enter OTP");
                        } else {
                          String confirmation = prefs.getString("otp") ?? "";
                          if (confirmation.length >= 4) {
                            if (confirmation == txtOtp.text.toString()) {
                              Navigator.of(context).pop();
                              sendSMS('$nametextholder', _product.split(':')[0].trim(), double.parse(_product.split(':').sublist(1).join(':').trim()));
                              txtpin.clear();
                            } else {
                              BaseClass.toast(
                                  "Incorrect verification code! Please try again");
                            }
                          }
                        }
                      } //submit,
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }
  sendSMS(String customerName, String lproducts,double lb) async {
    ProgressDialog prr= BaseClass.progreDialog(context);
    prr.show();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String agentname=prefs.getString("accname") ?? "";
    String message ="Hi "+customerName+ ", your outstanding loan balance for " +lproducts+" is "+lb.toString()+ ".\nYou were served by "+ agentname +". Thank you.";
    var url = Uri.parse(Network.URL + "SendTextMessage");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map otpBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporateno": prefs.getString("corporateno") ?? "",
      "telephone": '$phonetextholder',
      "text": message,
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
    };

    String body = json.encode(otpBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        BaseClass.alertDialog(
            context,
            "Success",
            "Your outstanding loan balance has been sent as message to "+'$phonetextholder',
            "Ok","assets/images/tick.jpg");

      } else {
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok","assets/images/error.jpg");
      }
    } else {
      BaseClass.alertDialog(context, "Failed",
          "Connection problem, please try again later...", "Ok","assets/images/error.jpg");
    }
  }
}

import 'package:agency_banking/components/LoanApplicationFormOnePannel.dart';
import 'package:agency_banking/components/LoanApplicationFormTwoPannel.dart';
import 'package:agency_banking/components/LoanApplicationMemberPannel.dart';
import 'package:flutter/material.dart';
class LoanApplication extends StatefulWidget {
  _LoanApplication createState() => _LoanApplication();
}

class _LoanApplication extends State<LoanApplication> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Loan Application',
          style: TextStyle(color: Colors.white),
        ),
      ),

      body:  Container(
        child: Column(
          children: [
            Expanded(
              child: Stepper(
                type: stepperType,
                physics: ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => tapped(step),
                onStepContinue:  continued,
                onStepCancel: cancel,
                steps: <Step>[
                  Step(
                    title: new Text('Member Details'),
                    content: LoanApplicationMemberPannel(),//beba camera: null,

                    isActive: _currentStep >= 0,
                    state: _currentStep >= 0 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Loan Application Form(1)'),
                    content: LoanApplicationFormOnePannel(),//beba camera: null,
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 1 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Loan Application Form(2)'),
                    content: LoanApplicationFormTwoPannel(),//hii ndo error: RenderCustomMultiChildLayoutBox object was given an infinite size during layout
                    isActive:_currentStep >= 0,
                    state: _currentStep >= 2 ?
                    StepState.complete : StepState.disabled,
                  ),

                ],
              ),
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.details),
        onPressed: switchStepsType,
      ),

    );
  }

  switchStepsType() {
    setState(() => stepperType == StepperType.vertical
        ? stepperType = StepperType.horizontal
        : stepperType = StepperType.vertical);
  }

  tapped(int step){
    setState(() => _currentStep = step);
  }

  continued(){
    _currentStep < 2 ?
    setState(() => _currentStep += 1): null;
  }
  cancel(){
    _currentStep > 0 ?
    setState(() => _currentStep -= 1) : null;
  }
}

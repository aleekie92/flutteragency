import 'package:agency_banking/components/RegistrationIDBackPhotoPannel.dart';
import 'package:agency_banking/components/RegistrationIDFrontPhotoPannel.dart';
import 'package:agency_banking/components/RegistrationNextOfKinPannel.dart';
import 'package:agency_banking/components/RegistrationPassportPhotoPannel.dart';
import 'package:agency_banking/components/RegistrationPersonalInfoPannel.dart';
import 'package:agency_banking/components/RegistrationPhysicalAddressPannel.dart';
import 'package:agency_banking/components/RegistrationRegFormPhotoOnePannel.dart';
import 'package:agency_banking/components/RegistrationRegFormPhotoTwoPannel.dart';
import 'package:agency_banking/components/RegistrationSignaturePhotoPannel.dart';
import 'package:flutter/material.dart';
class Registration extends StatefulWidget {
  _Registration createState() => _Registration();
}

class _Registration extends State<Registration> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Registration',
          style: TextStyle(color: Colors.white),
        ),
      ),

      body:  Container(

        child: Column(
          children: [
            Expanded(
              child: Stepper(
                type: stepperType,
                physics: ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => tapped(step),
                onStepContinue:  continued,
                onStepCancel: cancel,
                steps: <Step>[
                  Step(
                    title: new Text('Personal Information'),
                    content: RegistrationPersonalInfoPannel(),

                    isActive: _currentStep >= 0,
                    state: _currentStep >= 0 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('physical address'),
                    content: RegistrationPhysicalAddressPannel(),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 1 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('passport photo'),
                    content: RegistrationPassportPhotoPannel(),//hii ndo error: RenderCustomMultiChildLayoutBox object was given an infinite size during layout
                    isActive:_currentStep >= 0,
                    state: _currentStep >= 2 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('id front photo'),
                    content: RegistrationIDFrontPhotoPannel(),

                    isActive:_currentStep >= 0,
                    state: _currentStep >= 3 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('id back photo'),
                    content: RegistrationIDBackPhotoPannel(),

                    isActive:_currentStep >= 0,
                    state: _currentStep >= 4 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('signature photo'),
                    content: RegistrationSignaturePhotoPannel(),
                    isActive:_currentStep >= 0,
                    state: _currentStep >= 5 ?
                    StepState.complete : StepState.disabled,
                  ),
                  Step(
                    title: new Text('registration form photo(leavelet 1)'),
                    content: RegistrationRegFormPhotoOnePannel(),

                    isActive:_currentStep >= 0,
                    state: _currentStep >= 6 ?
                    StepState.complete : StepState.disabled,
                  ),

                  Step(
                    title: new Text('registration form photo(leavelet 2)'),
                    content: RegistrationRegFormPhotoTwoPannel(),

                    isActive:_currentStep >= 0,
                    state: _currentStep >= 7 ?
                    StepState.complete : StepState.disabled,
                  ),

                  Step(
                    title: new Text('next of kin details'),
                    content: RegistrationNextOfKinPannel(),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 8 ?
                    StepState.complete : StepState.disabled,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
 floatingActionButton: FloatingActionButton(
        child: Icon(Icons.details),
        onPressed: (){
          SubmitRegistration();
          },
      ),
    );
  }

  tapped(int step){
    setState(() => _currentStep = step);
  }

  continued(){
    _currentStep < 2 ?
    setState(() => _currentStep += 1): null;
  }
  cancel(){
    _currentStep > 0 ?
    setState(() => _currentStep -= 1) : null;
  }

  SubmitRegistration(){

  }
}











/*
import 'package:agency_banking/components/RegistrationPersonalInfoPannel.dart';
import 'package:agency_banking/components/RegistrationPhysicalAddressPannel.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';

class Registration extends StatefulWidget {
  @override
  _Registration createState() => _Registration();
}

class _Registration extends State<Registration> {
  int activeStep = 0;
  int upperBound = 9;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              IconStepper(
                icons: [
                  Icon(Icons.person),
                  Icon(Icons.location_on),
                  Icon(Icons.perm_identity),
                  Icon(Icons.card_giftcard_outlined),
                  Icon(Icons.card_giftcard_sharp),
                  Icon(Icons.assignment_late_outlined),
                  Icon(Icons.app_registration),
                  Icon(Icons.app_registration),
                  Icon(Icons.format_list_numbered),
                ],
                activeStep: activeStep,
                onStepReached: (index) {
                  setState(() {
                    activeStep = index;
                  });
                },
              ),
              header(),
              Expanded(
                child: FittedBox(
                  child: Center(
                    // child: Text('$activeStep'),
                    child: Row(
                      children: [
                        body(),
                        //nextButton(),
                      ],
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  previousButton(),
                  nextButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Returns the next button.
  Widget nextButton() {
    return ElevatedButton(
      onPressed: () {
        // Increment activeStep, when the next button is tapped. However, check for upper bound.
        if (activeStep < upperBound) {
          setState(() {
            activeStep++;
          });
        }
      },
      child: Text('Next'),
    );
  }

  /// Returns the previous button.
  Widget previousButton() {
    return ElevatedButton(
      onPressed: () {
        // Decrement activeStep, when the previous button is tapped. However, check for lower bound i.e., must be greater than 0.
        if (activeStep > 1) {
          setState(() {
            activeStep--;
          });
        }
      },
      child: Text('Prev'),
    );
  }

  /// Returns the header wrapping the header text.
  Widget header() {
    return Container(
      decoration: BoxDecoration(
        color: kPrimaryColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              headerText(),
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget body() {
    if (activeStep == 1) {
      RegistrationPersonalInfoPannel();
    } else if (activeStep == 2) {
      RegistrationPhysicalAddressPannel();
    }
  }

  // Returns the header text based on the activeStep.
  String headerText() {
    switch (activeStep) {
      case 1:
        return 'Personal Information';

      case 2:
        return 'physical address';

      case 3:
        return 'passport photo';

      case 4:
        return 'id front photo';

      case 5:
        return 'id back photo';

      case 6:
        return 'signature photo';
      case 7:
        return 'Application Form (Page1)';
      case 8:
        return 'Application Form (Page2)';
      case 9:
        return 'Next of Kin';

      default:
        return 'Introduction';
    }
  }
}

*/

import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AgentAccount extends StatefulWidget {
  _AgentAccount createState() => _AgentAccount();
}

class _AgentAccount extends State<AgentAccount>{
  bool visible = false;
  String sacconamee,agtacc,bussname,busslocation,agtaccbal,agtaccname;
  Future<dynamic> savedAgentData;

  @override
  void initState() {
    savedAgentData=agentAccount();
    //agentAccount();
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My Account',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: visible,
                    child: CircularProgressIndicator(
                      backgroundColor: colorPrimary,
                      strokeWidth: 2,
                    ),
                  ),

                  CircleAvatar(
                    radius: 40,
                    backgroundImage: AssetImage("assets/images/user.jpg"),
                  ),
                  getAgentDetails
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
    Future<dynamic> agentAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sacconamee =prefs.getString("sacconame") ?? "";
    agtaccname=prefs.getString("accname") ?? "";
    bussname=prefs.getString("businessname") ?? "";
    agtacc=prefs.getString("agentaccount") ?? "";
    busslocation=prefs.getString("bussinesslocation") ?? "";
    agtaccbal=(prefs.getDouble("agtaccbalance") ?? "").toString();

    Map values = {'sacconame': sacconamee, 'accname': agtaccname,
      'businessname': bussname,"agentaccount":agtacc,"bussinesslocation":busslocation,"agtaccbalance":agtaccbal};
    return values;
  }

  get getAgentDetails{
    return FutureBuilder(future:agentAccount(), builder: (BuildContext context, AsyncSnapshot snapshot) {
      if(snapshot.data==null){
        return SizedBox(height: 0,);
      }
      else{
        return Column(
          children: [
            Text(
              snapshot.data['sacconame'].toUpperCase(),
              style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            new ListTile(
              leading: const Icon(Icons.perm_identity),
              title: Row(
                children: [
                  new Text("Operator :".toUpperCase(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                  //Spacer(),
                  new Text( snapshot.data['accname'].toUpperCase(),style: TextStyle(fontSize: 11),),
                ],
              ),
            ),
            new ListTile(
              leading: const Icon(Icons.account_balance),
              title: Row(
                children: [
                  new Text("Business name :".toUpperCase(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold)),
                  //Spacer(),
                  new Text(snapshot.data['businessname'],style: TextStyle(fontSize: 11),),
                ],
              ),
            ),
            new ListTile(
              leading: const Icon(Icons.account_balance_wallet_outlined),
              title: Row(
                children: [
                  new Text("account number :".toUpperCase(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                  //Spacer(),
                  new Text(snapshot.data['agentaccount'],style: TextStyle(fontSize: 11),),
                ],
              ),
            ),
            new ListTile(
              leading: const Icon(Icons.compass_calibration),
              title: Row(
                children: [
                  new Text("location :".toUpperCase(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                  //Spacer(),
                  new Text(snapshot.data['bussinesslocation'],style: TextStyle(fontSize: 11),),
                ],
              ),
            ),
            new ListTile(
              leading: const Icon(Icons.monetization_on_outlined),
              title: Row(
                children: [
                  new Text("account balance :".toUpperCase(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                  //Spacer(),
                  new Text(snapshot.data['agtaccbalance'],style: TextStyle(fontSize: 11),),
                ],
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
          ],
        );
      }
    });
  }
}

import 'dart:convert';
import 'dart:typed_data';

import 'package:agency_banking/components/HeaderWithSearchBox.dart';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'file:///F:/projects/android/agency_banking/lib/constants/printjob.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Withdraw extends StatefulWidget {
  _Withdraw createState() => _Withdraw();
}

class _Withdraw extends State<Withdraw> {
  TextEditingController txtID = new TextEditingController();
  TextEditingController txtAmount = new TextEditingController();
  static TextEditingController txtpin = new TextEditingController();
  TextEditingController txtOtp = new TextEditingController();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  bool visible = false;
  bool submitprogressbar = false;
  String nametextholder = "***********";
  String phonetextholder = "***********";
  List<String> accountNoList = [];
  List<String> phoneNoList = [];

  String photoo, signaturee;
  Uint8List photoImage;
  Uint8List photoSignature;
  int count = 0;

  @override
  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> dataList) {
    List<DropdownMenuItem<String>> accountItems = new List();
    if (dataList.isNotEmpty) {
      for (String acc in dataList) {
        accountItems
            .add(new DropdownMenuItem(value: acc, child: new Text(acc)));
      }
      return accountItems;
    } else {
      print("seeing null");
    }
  }

  String _account;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Cash Withdrawal',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: new Column(
                children: <Widget>[
                  memberDetails,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  get memberDetails {
    return Column(children: [
      new ListTile(
        title: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                obscureText: false,
                keyboardType: TextInputType.number,
                maxLength: 10,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                controller: txtID,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter ID number".toUpperCase(),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.white,
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                if (txtID.text.isEmpty) {
                  BaseClass.toast("Please enter account Identifier");
                } else if (txtID.text.length < 5) {
                  BaseClass.toast("Please enter a valid account Identifier");
                } else {
                  setState(() {
                    visible = true;
                  });
                  searchFuntionality();
                }
              },
              child: Image(
                image: AssetImage("assets/images/search.png"),
              ), //const Icon(Icons.search),
            )
          ],
        ),
      ),
      memberNameandPhone,
      //loadPhotoAndSignature,
      new ListTile(
        title: Padding(
          padding: const EdgeInsets.only(
              left: kDefaultPadding + 5, right: kDefaultPadding + 5),
          child: Row(
            children: [
              CircleAvatar(
                radius: 40,
                backgroundImage: AssetImage("assets/images/user.jpg"),
              ),
              Spacer(),
              CircleAvatar(
                radius: 40,
                backgroundImage: AssetImage("assets/images/signature.png"),
              )
            ],
          ),
        ),
      ),
      Text(
        "Select account",
        style: TextStyle(
            color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18),
      ),
      loadDataAccounts,
      new ListTile(
        leading: const Icon(Icons.monetization_on_outlined),
        title: new TextField(
          controller: txtAmount,
          keyboardType: TextInputType.number,
          decoration: new InputDecoration(
            hintText: "Enter Amount",
          ),
        ),
      ),
      SizedBox(
        height: 15.0,
      ),
      new Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: colorPrimary,
        child: MaterialButton(
          minWidth: 150, //MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () async {
            if (txtAmount.text.isEmpty ||
                nametextholder == "***********") {
              BaseClass.toast("please Key in the missing fields.");
            } else if (_account == null) {
              BaseClass.toast("please select account number.");
            } else {
              authDialog(context, txtID.text.toString());
            }
          },
          child: Text("Submit".toUpperCase(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    ]);
  }

  showProgress() {
    return Visibility(
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      visible: visible,
      child: CircularProgressIndicator(
        backgroundColor: colorPrimary,
        strokeWidth: 2,
      ),
    );
  }

  Future<void> searchFuntionality() async {
    var response =
        await BaseClass.fetchMemberDetails(txtID.text.toString(), 0, context);
    try {
      if (response != null) {
          nametextholder = response['memberName'].toString();
          phonetextholder = response['memberPhone'].toString();
          setState(() {
            visible = false;
          });
          BaseClass.enableBluetooth();
      } else {
        setState(() {
          visible = false;
        });
        BaseClass.toast("Member details not found, please try again...");
      }
    } catch (Ex) {
      setState(() {
        visible = false;
      });
      BaseClass.toast("Member details not found, please try again...");
    }
  }

  get memberNameandPhone {
    return Column(children: [
      new ListTile(
        leading: const Icon(Icons.format_list_numbered_outlined),
        title: Row(
          children: [
            new Text(
              nametextholder.toUpperCase(),
              overflow: TextOverflow.fade,
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor1),
            ),
            Spacer(),
             showProgress(),
          ],
        ),
      ),
      new ListTile(
        leading: const Icon(Icons.phone),
        title: Row(
          children: [
            new Text("Phone :"),
            //Spacer(),
            new Text(phonetextholder),
          ],
        ),
      ),
    ]);
  }

  get loadDataAccounts {
    String id = txtID.text.toString();
    if (id.isNotEmpty) {
      return FutureBuilder(
          future: BaseClass.requestMemberAccounts(context, id, "W"),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );
            } else {
              try {
                accountNoList.clear();
                phoneNoList.clear();
                for (int i = 0;
                    i < snapshot.data["saccoaccounts"].length;
                    i++) {
                  for (int j = 0;
                      j <
                          snapshot.data["saccoaccounts"][i]["accountdetails"]
                              .length;
                      j++) {
                    accountNoList.add(snapshot.data["saccoaccounts"][i]
                            ["accountdetails"][j]["accountno"] +
                        " - " +
                        snapshot.data["saccoaccounts"][i]["accountdetails"][j]
                            ["accounttype"]);
                    phoneNoList.add(snapshot.data["saccoaccounts"][i]
                        ["accountdetails"][j]["phonenumber"]);
                  }
                }
                return new DropdownButton(
                  value: _account,
                  items: getDropDownMenuItems(accountNoList),
                  onChanged: productsChangedDropDownItem,
                );
              } catch (e) {
                return Text(snapshot.data.toString());
              }
            }
          });
    } else {
      return new DropdownButton(
        value: _account,
        items: getDropDownMenuItems(accountNoList),
        onChanged: productsChangedDropDownItem,
      );
    }
  }

  submitWithdrawal(int amount, String id, String accNo) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String terminalid = prefs.getString("imei") ?? "";
    String cooporateNo = prefs.getString("corporateno") ?? "";
    String agentId = prefs.getString("agtcode") ?? "";
    String motoo = prefs.getString("saccomoto") ?? "";

    var url = Uri.parse(Network.URL + "AgentFundsWithdrawal");
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map withdrawalBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "corporate_no": cooporateNo,
      "amount": amount,
      "accountidentifier": id,
      "accountno": accNo.split('-')[0].trim(),
      "iscashwithdrawal": true,
      "accountidentifiercode": "0",
      "agentid": agentId,
      "terminalid": terminalid,
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(withdrawalBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      prr.hide();
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        prr.hide();
        String custName = responseData['customername'];
        String rNo = responseData['receiptno'];
        String sName = responseData['sacconame'];
        String tDate = responseData['transactiondate'];
        confirmdialog(context, accNo, amount, custName, terminalid, rNo, sName,
            tDate, motoo, id);
      } else {
        prr.hide();
        BaseClass.alertDialog(context, "Error!",
            "Error :" + responseData['error'], "Ok", "assets/images/error.jpg");
      }
    }
    prr.hide();
  }

  void productsChangedDropDownItem(String selectedAccount) {
    setState(() {
      _account = selectedAccount;
    });
  }

  authDialog(BuildContext context, String id) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 220,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Enter your Initial activation pin to proceed.".toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                maxLength: 4,
                controller: txtpin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Pin",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: colorPrimary,
                      onPressed: () {
                        txtpin.clear();
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'dashboard', (route) => false);
                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        if (txtpin.text.isEmpty) {
                          BaseClass.toast("Enter pin");
                        } else {
                          memberAuthRequest(context, id);
                          /*var responsee = await BaseClass.generatePINAndSend(
                              context, '$phonetextholder');

                          BaseClass.enableBluetooth();
                          if (responsee != null) {
                            BaseClass.toast("OTP has been send to " + '$phonetextholder');
                            memberAuthRequest(context, id);
                          } else {
                            BaseClass.toast("Failed, please try again....");
                          }*/
                        }
                      }
                      ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }

  memberAuthRequest(BuildContext context, String id) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    var url = Uri.parse(Network.URL + "AuthenticateCustomerLogin");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };

    Map memberAuthBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "pin": txtpin.text.toString(),
      "accountidentifier": id,
      "accountidentifiercode": "0",
      "agentid": prefs.getString("agtcode") ?? "",
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };

    String body = json.encode(memberAuthBlock);

    Response response = await post(url, headers: header, body: body);
    setState(() {
      submitprogressbar = true;
    });
    if (response != null) {
      var responseData = jsonDecode(response.body);
      setState(() {
        submitprogressbar = false;
      });
      if (responseData['is_successful']) {
        prr.hide();
        setState(() {
          submitprogressbar = false;
        });
        var responsee = await BaseClass.generatePINAndSend(
            context, '$phonetextholder');

        BaseClass.enableBluetooth();
        if (responsee != null) {
          BaseClass.toast("OTP has been send to " + '$phonetextholder');
          oneTimePinDialog(context);
        } else {
          BaseClass.toast("Failed, please try again....");
        }
         txtpin.clear();
      } else {
        setState(() {
          submitprogressbar = false;
        });
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Ok",
            "assets/images/error.jpg");
        txtpin.clear();
      }
    } else {
      setState(() {
        submitprogressbar = false;
      });
      BaseClass.alertDialog(context, "Failed", "Please try again", "Ok",
          "assets/images/error.jpg");
      txtpin.clear();
    }
  }

  get loadPhotoAndSignature {
    String id = txtID.text.toString();
    if (id.isNotEmpty) {
      return FutureBuilder(
          future: BaseClass.getPicture(id, context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator(
                backgroundColor: Colors.red,
              );
            } else {
              photoo = snapshot.data["photosign"]["photo"];
              signaturee = snapshot.data["photosign"]["signature"];
              try {
                return new ListTile(
                  title: Padding(
                    padding: const EdgeInsets.only(
                        left: kDefaultPadding + 5, right: kDefaultPadding + 5),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 40,
                          backgroundImage: AssetImage("assets/images/user.png"),
                        ),
                        Spacer(),
                        CircleAvatar(
                          radius: 40,
                          backgroundImage:
                              AssetImage("assets/images/signature.png"),
                        )
                      ],
                    ),
                  ),
                );
              } catch (e) {
                print("something");
              }
            }
          });
    } else {
      print("something");
    }
  }

  oneTimePinDialog(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 200,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Enter verification code sent to " +
                    '$phonetextholder'.toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtOtp,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter OTP",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        Navigator.of(context).pushNamedAndRemoveUntil('dashboard', (route) => false);
                        txtpin.clear();
                        txtOtp.clear();

                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        if (txtOtp.text.isEmpty) {
                          BaseClass.toast("Enter OTP");
                        } else {
                          String confirmation = prefs.getString("otp") ?? "";
                          if (confirmation.length >= 4) {
                            if (confirmation == txtOtp.text.toString()) {
                              submitWithdrawal(
                                  int.parse(txtAmount.text.toString()),
                                  txtID.text.toString(),
                                  _account);
                              txtpin.clear();
                              txtOtp.clear();
                            } else {
                                count++;
                              if (count == 1) {
                                BaseClass.toast("Incorrect verification code! two trials remaining");
                              }else if(count == 2){
                                BaseClass.toast("Incorrect verification code! one trial remaining");
                              }else if(count == 3){
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    'dashboard', (route) => false);
                                txtpin.clear();
                                BaseClass.toast("you have entered incorrect OTP three times, please try again later");
                              }
                            }
                          }
                        }
                      } //submit,
                      ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }

  confirmdialog(
      BuildContext context,
      String accNo,
      int amount,
      String custName,
      String terminalid,
      String rNo,
      String sName,
      String tDate,
      String motoo,
      String id) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog successdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Stack(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
              height: 220,
              width: size.width * 3,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 45.0),
                  Text(
                    "Cash withdrawal submitted successfully.",
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 15.0),
                  Text(
                    "Would you wish to print receipt?".toUpperCase(),
                    style: TextStyle(color: Colors.blue),
                  ),
                  SizedBox(height: 30.0),
                  new Row(
                    children: <Widget>[
                      RaisedButton(
                          child: Text(
                            "No",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: kPrimaryColor,
                          onPressed: () async {
                            //Navigator.of(context).pop();
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                'dashboard', (route) => false);
                          }
                          //submit,
                          ),
                      Spacer(),
                      RaisedButton(
                          child: Text(
                            "Yes",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PrintDemo(
                                      transWhat: "W",
                                      memberno: accNo.split('-')[0].trim(),
                                      amt: amount,
                                      title: "AGENT CASH WITHDRAWAL",
                                      depositorphone: "",
                                      depositorname: custName,
                                      deviceid: terminalid,
                                      agentname: agentName,
                                      refno: rNo,
                                      sacconame: sName,
                                      idNo: id,
                                      transactiondate: tDate,
                                      decription: "",
                                      depbyy: "",
                                      moto: motoo,
                                      accountname: '$nametextholder'),
                                ));
                          }
                          //submit,
                          ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  )
                ],
              )),
          Positioned(
            left: kDefaultPadding,
            right: kDefaultPadding,
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: kDefaultPadding,
              child: ClipRRect(
                  borderRadius:
                      BorderRadius.all(Radius.circular(kDefaultPadding)),
                  child: Image.asset("assets/images/tick.jpg")),
            ),
          ),
        ],
      ),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => successdialog);
  }
}

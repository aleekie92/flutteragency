import 'dart:convert';
import 'dart:math';
import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  TextEditingController txtagentcode = new TextEditingController();
  TextEditingController txtpin = new TextEditingController();
  TextEditingController txtOtp = new TextEditingController();
  TextEditingController btnLogin = new TextEditingController();
  TextEditingController txtOldPin = new TextEditingController();
  TextEditingController txtNewPin = new TextEditingController();
  TextEditingController txtConfirmPin = new TextEditingController();
  bool visible = false;
  bool isEnabled = true;
  bool passChanged;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final agentcode = TextField(
      obscureText: false,
      style: style,
      controller: txtagentcode,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.characters,
      maxLength: 11,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Agent Code",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final agentpin = TextField(
      obscureText: true,
      style: style,
      controller: txtpin,
      maxLength: 4,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Pin",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: colorPrimary,
      child: MaterialButton(
        minWidth: 250, //MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          if (txtagentcode.text.isEmpty || txtpin.text.isEmpty) {
            setState(() {
              visible = false;
            });
            BaseClass.toast("Input the missing field to continue");
          } else if (txtagentcode.text.length <= 5 ||
              txtagentcode.text.length >= 13) {
            setState(() {
              visible = false;
            });
            BaseClass.toast("invalid agent code");
          } else if (txtpin.text.length < 4) {
            BaseClass.toast("enter a valid pin");
          } else {
            login();
            setState(() {
              visible = true;
            });
          }
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return new WillPopScope(
      onWillPop: () async {
        BaseClass.toast(
            "minimise the application and clear to close completely");
        return false;
      },
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 45.0),
                    SizedBox(
                        child: Image(
                      image: AssetImage("assets/images/logoo.png"),
                    )),
                    Visibility(
                      maintainSize: true,
                      maintainAnimation: true,
                      maintainState: true,
                      visible: visible,
                      child: CircularProgressIndicator(
                        backgroundColor: colorPrimary,
                        strokeWidth: 3,
                      ),
                    ),
                    agentcode,
                    agentpin,
                    loginButon,
                    SizedBox(
                        //height: 15.0,
                        ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  login() async {
    var url = Uri.parse(Network.URL + "AgentAuthentication");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };
    Map loginBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "agentcode": txtagentcode.text,
      "agentpin": txtpin.text,
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? "")
    };
    String body = json.encode(loginBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      setState(() {
        visible = false;
      });
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        await prefs.setString('agtcode', txtagentcode.text);
        passChanged = responseData['passwordchanged'];
        if(txtagentcode.text.toString().substring(0,3)=="SAC"){

          var responsee = await BaseClass.generatePINAndSend(
              context, responseData['phonenumber']);
          if (responsee != null) {
            if (!passChanged) {
              createDialogChangePassword(context);
            } else {
              String agtphonee=responseData['phonenumber'];
              String splitagtphonee=agtphonee.substring(agtphonee.length - 9);
              await prefs.setString('agtphone', splitagtphonee);
              oneTimePinDialog(context);
            }
          } else {
            BaseClass.toast("Failed, please try again....");
          }
          setState(() {
            visible = false;
          });
        }else{
          if (!passChanged) {
            createDialogChangePassword(context);
          } else {
            String agtphonee=responseData['phonenumber'];
            String splitagtphonee=agtphonee.substring(agtphonee.length - 9);
            await prefs.setString('agtphone', splitagtphonee);
            Navigator.pushNamed(context, "dashboard");
          }
        }

      } else {
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Close",
            "assets/images/error.jpg");
        setState(() {
          visible = false;
        });
      }
    } else {
      BaseClass.alertDialog(
          context,
          "Failed",
          "Server error, please try again..",
          "Close",
          "assets/images/error.jpg");
      setState(() {
        visible = false;
      });
    }
  }

  oneTimePinDialog(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 220,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Enter Verification Code sent you".toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 30.0),
              TextField(
                obscureText: true,
                style: style,
                maxLength: 4,
                controller: txtOtp,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter Code",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 15.0),
              new Row(
                children: <Widget>[
                  RaisedButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: colorPrimary,
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context)
                            .pushNamedAndRemoveUntil('login', (route) => false);
                      }),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Okay",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        if (txtOtp.text.isEmpty) {
                          BaseClass.toast("Enter OTP");
                        } else {
                          String confirmation = prefs.getString("otp") ?? "";
                          if (confirmation.length >= 4) {
                            if (confirmation == txtOtp.text.toString()) {
                              Navigator.of(context).pop();
                              Navigator.pushNamed(context, "dashboard");
                            } else {
                              BaseClass.toast(
                                  "Incorrect verification code! Please try again");
                            }
                          }
                        }
                      } //submit,
                      ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }

  createDialogChangePassword(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    Dialog authdialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kDefaultPadding),
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: 280,
          width: size.width * 3,
          child: Column(
            children: <Widget>[
              Text(
                "Change Agent Pin".toUpperCase(),
                style: TextStyle(color: Colors.blue),
              ),
              SizedBox(height: 20.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtOldPin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter initial pin",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtNewPin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Enter new pin",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                style: style,
                controller: txtConfirmPin,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Confirm new pin",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 10.0),
              new Row(
                children: <Widget>[

                  RaisedButton(
                      child: Text(
                        "Proceed",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () async {
                        if (txtOldPin.text.isEmpty ||
                            txtNewPin.text.isEmpty ||
                            txtConfirmPin.text.isEmpty) {
                          BaseClass.toast("Enter Missing values to proceed");
                        } else if (txtOldPin.text.toString() !=
                            txtpin.text.toString()) {
                          BaseClass.toast("incorrect initial Pin");
                        } else if (txtNewPin.text.toString() !=
                            txtConfirmPin.text.toString()) {
                          BaseClass.toast("confirm pin did not match");
                        } else {
                          Navigator.of(context).pop();
                          requestPasswordChange(txtOldPin.text.toString(),
                              txtConfirmPin.text.toString());
                        }
                      }
                      //submit,
                      ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => authdialog);
  }

  requestPasswordChange(String oldpin, String newPin) async {
    ProgressDialog prr = BaseClass.progreDialog(context);
    prr.show();
    var url = Uri.parse(Network.URL + "ChangePinAgent");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {
      "Content-Type": "application/json; charset=utf-8"
    };
    Map loginBlock = {
      "authorization_credentials": {
        "api_key": "12345",
        "token": Network.appToken
      },
      "old_pin": oldpin,
      "new_pin": newPin,
      "accountidentifier": txtagentcode.text,
      "accountidentifiercode": "0",
      "agentid": txtagentcode.text,
      "terminalid": prefs.getString("imei") ?? "",
      "longitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("longitude") ?? ""),
      "latitude": BaseClass.getLatandLong(),// double.parse(prefs.getString("latitude") ?? ""),
      "date": BaseClass.getFormattedDate()
    };
    String body = json.encode(loginBlock);
    Response response = await post(url, headers: header, body: body);
    if (response != null) {
      setState(() {
        visible = false;
      });
      var responseData = jsonDecode(response.body);
      if (responseData['is_successful']) {
        prr.hide();
        BaseClass.alertDialog(context, "Success",
            "Agent Pin Changed Successfully. ", "Ok", "assets/images/tick.jpg");
      } else {
        prr.hide();
        BaseClass.alertDialog(context, "Failed", responseData['error'], "Close",
            "assets/images/error.jpg");
        setState(() {
          visible = false;
        });
      }
    } else {
      prr.hide();
      BaseClass.alertDialog(
          context,
          "Failed",
          "Server error, please try again..",
          "Close",
          "assets/images/error.jpg");
      setState(() {
        visible = false;
      });
    }
  }
}

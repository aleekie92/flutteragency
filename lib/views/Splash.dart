import 'package:agency_banking/constants/BaseClass.dart';
import 'package:agency_banking/constants/Network.dart';
import 'package:agency_banking/constants/Utils.dart';
import 'package:android_intent/android_intent.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:permission_handler/permission_handler.dart';

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Splash();
}

class _Splash extends State<Splash> {
  @override
  void initState() {
    //getNetworkStatus();
    super.initState();
    requestPermissionLocation();
  }

  void requestPermissionLocation() async {
    try {
      if (await Permission.location.isDenied) {
        await Permission.location.request();
        PermissionStatus status = await Permission.location.status;
      }
      if (await Permission.location.isGranted) {
        _gpsService();
      } else {
        await _checkGps();
      }
    } catch (e) {
      BaseClass.alertDialog(context, "Failed", "Enable location Permisions",
          "Close", "assets/images/error.jpg");
    }
  }

  Future _checkGps() async {
    if (!await Geolocator().isLocationServiceEnabled()) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Can't get current location"),
                content:
                    const Text('Please make sure you enable GPS and try again'),
                actions: <Widget>[
                  FlatButton(
                      child: Text('Enable'),
                      onPressed: () async {
                        Navigator.of(context).pop();
                        final AndroidIntent intent = AndroidIntent(
                            action:
                                'android.settings.LOCATION_SOURCE_SETTINGS');
                        intent.launch();
                        //await getImei();
                      })
                ],
              );
            });
      }
    } else {
      await _gpsService();
    }
  }

  void _gpsService() async {
    if (await Geolocator().isLocationServiceEnabled()) {
      await getImei();
    } else {
      _checkGps();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorPrimary,
      child: Image(
        image: AssetImage("assets/images/logo.png"),
      ),
    );
  }

  navigate() async {
    await Network.generateToken("GenerateToken", context);
    if (Network.appToken != null) {
      Future.delayed(Duration(seconds: 5), () {
        //_getCordinates();
        Navigator.pushNamed(context, "login");
      });
    } else {
      BaseClass.alertDialog(context, "Allah!", "Weak connection, try again",
          "Exit", "assets/images/error.jpg");
    }
  }

  Future<void> getImei() async {
    await Permission.phone.request();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String platformImei = await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;

    bool isRooted = androidDeviceInfo.isRooted;
    String androidid = androidDeviceInfo.androidId;

    if (isRooted) {
      BaseClass.alertDialog(context, "Alert!", "Your device is rooted", "Exit",
          "assets/images/error.jpg");
    }

    if (platformImei == "unknown") {
      if (androidid.length >= 29) {//i.e 29++
        await prefs.setString("imei", androidid.substring(0, 29));
        navigate();
      } else {
        await prefs.setString("imei", androidid);
        navigate();
      }

    } else {
      if (androidid.length >= 29) {
        await prefs.setString("imei", platformImei.substring(0, 29));
        navigate();
      } else {
        await prefs.setString("imei", platformImei);
        navigate();
      }
    }
  }

  _getCordinates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    await prefs.setString('longitude', position.longitude.toString());
    await prefs.setString('latitude', position.latitude.toString());
  }
}
